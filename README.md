# MMMultiply 2018

The MMMultiply protfolio site. 

A lightweight html based site, no db or cms is used. The site is built with a php framework and uses ajax page loads for smooth transitions.

Every page has 2 files, a 'PageName'.php and a 'PageName'-content.php, The first contains links to the header and footer modules and the -content part, the -content.php file contains the page contents. The reason for this is so that an ajax call from another page can retrieve just the new contents that need to be replaced within the page.

page parts:
[header-html]
[header contents]
[page contents]
[footer-contents]
[footer-html]

So the only part that is replaced in an ajax call is the [page contents].


## Getting Started

The development environment runs with GULP, this will start a server at http://localhost:3000 and compile all the code into the /assets folder.

###Dev Environment:

install node.js if you dont not allready have it (https://nodejs.org/en/)

In the terminal app, navigate to the site folder using "cd" and "ls" commands, then run:
(you may need the use a "sudo" command infront of the following line to gain admin privileges )

```
npm install
```


Once all of the dependencies are installed run Gulp in terminal, for more information (https://gulpjs.com/)

```
gulp
```

This should start the server with browser sync, and watch the code base for changes and compile new code every time a file changes.
   

###Build structure:    

The server root is the /assets folder. The build files are located at the project root.


###Css:    

The project usses Compass to compile .scss files into a core.css file. If you dont want to use .scss. Normal css can be used as well.


###Adding a new project:    

To add a new project create a 'PageName'.php following the links in another base level page and a 'PageName'-content.php with the specific page content in here. 


###Updating the work page:    
Duplicate an existing link to a project and update the conents, see next section for information on updating an ajax link.


###AJAX links

An ajax link should look like this
```
<a href="pageName.php" data-pageid="content-page" class="ajax-link">
	***content***
</a>
```

Please note the data-pageid attribute. This is very important as the adds a class to the page body which applies styles to the document. 

page classes are: 
'index-content' = home page
'about-page' = about page
'projects-page' = work page
'studio-page' = studio
'content-page' = work content/privacy policy

