<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Mars S2 Campaign</h1>
			<p>
				Mars is a documentary and science fiction television series produced by National Geographic. 
			</p>
			<p>
			It blends elements of real interviews with a fictional story of a group of astronauts as they land on the planet in the not too distant future. The show was premiering on Nat Geo in the UK and we were asked to help with the ATL campaign rollout. This included a large DOOH campaign on day of TX as well as various digital units.
			</p>

		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">



				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-Mars-01.jpg" data-lightbox="Mars">
						<img src="img/MMMultiply-Mars-01.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-Mars-03.jpg" data-lightbox="Mars">
						<img src="img/MMMultiply-Mars-03.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-Mars-02.jpg" data-lightbox="Mars">
						<img src="img/MMMultiply-Mars-02.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-Mars-04.jpg" data-lightbox="Mars">
						<img src="img/MMMultiply-Mars-04.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Mars-07.jpg" data-lightbox="Mars">
						<img src="img/MMMultiply-Mars-07.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Mars-06.jpg" data-lightbox="Mars">
						<img src="img/MMMultiply-Mars-06.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-Mars-05.jpg" data-lightbox="Mars">
						<img src="img/MMMultiply-Mars-05.jpg">
					</a>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="wild-launch-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Wild Launch Campaign</span>
				<img src="img/MMMultiply-WildLaunchCampaign-02.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State S1 Campaign</span>
				<img src="img/MMMultiply-DeepStateATL-01.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-s2-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State S2 Campaign</span>
				<img src="img/MMMultiply-DeepStateS2-KA-Thumb.jpg">
			</a>
		</div>
	</div>
</section>