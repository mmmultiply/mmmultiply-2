<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Heroes & Villains Promo</h1>
			<p>
				A heroic homage for Sky Cinema’s villainously brilliant new channels, Sky Cinema Heroes and Sky Cinema Villains.
			</p>
			<p>
				Working closely with the team at Sky Creative we produced this promo homage to Heroes and Villains of cinema. We created iconic imagery from a selection of the films available and then seamlessly transitioned between them. This coupled with a fantastic audio bed of iconic sound bites and SFX made for a stunning promo that really stands out from standard movie montage edits.	
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">
				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/257115699" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load inview">

					<a href="img/MMMultiply-HeroesVillains-01.jpg" data-lightbox="Heroes villains">
						<img src="img/MMMultiply-heroesVillains-01.jpg">
					</a>

				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-HeroesVillains-02.jpg" data-lightbox="Heroes villains">
						<img src="img/MMMultiply-heroesVillains-02.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-HeroesVillains-03.jpg" data-lightbox="Heroes villains">
						<img src="img/MMMultiply-heroesVillains-03.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-HeroesVillains-04.jpg" data-lightbox="Heroes villains">
						<img src="img/MMMultiply-heroesVillains-04.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-HeroesVillains-05.jpg" data-lightbox="Heroes villains">
						<img src="img/MMMultiply-heroesVillains-05.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-HeroesVillains-06.jpg" data-lightbox="Heroes villains">
						<img src="img/MMMultiply-heroesVillains-06.jpg">
					</a>
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-3 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="superheroes.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes-05-thumb.jpg" data-filter="motion">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="sky-cinema-superheroes-2019.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes 2019</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes2019-01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="suicide-squad.php" data-pageid="content-page" class="ajax-link">
				<span>Suicide Squad Packaging</span>
				<img src="img/MMMultiply-SS-Cover.jpg">
				
			</a>
		</div>
	</div>
</section>