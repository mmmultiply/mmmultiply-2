<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">National Geographic+</h1>
			<p>
				National Geographic came to us to with a branding brief for the launch of their new on-demand service National Geographic+.
			</p>
			<p>
				Our challenge was to design a brand identity in line with their recent channel rebrand but expand on this for viewers to be able to clearly distinguish this as a new platform where they could access all of their favourite National Geographic content. The theme ‘Explore More’, aimed to engage a younger demographic, perpetuating the ‘Attenborough effect’ to highlight awareness and push discussions through campaign advertising and social media.
			</p>

		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">
				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/336123486?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-02.gif" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-02.gif">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-08.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-08.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-03.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-03.jpg">
					</a>
				</div>



				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-05.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-05.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-07.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-07.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-09.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-09.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-04.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-04.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-10.jpg" data-lightbox="NatGeoPlus">
						<img  style="border: 1px solid #CCC" src="img/MMMultiply-NatGeo-Plus-10.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-11.jpg" data-lightbox="NatGeoPlus">
						<img  style="border: 1px solid #CCC" src="img/MMMultiply-NatGeo-Plus-11.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-12.jpg" data-lightbox="NatGeoPlus">
						<img  style="border: 1px solid #CCC" src="img/MMMultiply-NatGeo-Plus-12.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-13.jpg" data-lightbox="NatGeoPlus">
						<img  style="border: 1px solid #CCC" src="img/MMMultiply-NatGeo-Plus-13.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-06.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-06.jpg">
					</a>
				</div>


				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-NatGeo-Plus-01.jpg" data-lightbox="NatGeoPlus">
						<img src="img/MMMultiply-NatGeo-Plus-01.jpg">
					</a>
				</div>

			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="national-geographic-magazine.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Magazine</span>
				<img src="img/MMMultiply-NatGeo-Mag-10.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="wild-launch-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Wild Launch Campaign</span>
				<img src="img/MMMultiply-WildLaunchCampaign-02.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="fox-plus.php" data-pageid="content-page" class="ajax-link">
				<span>FOX+ Brand</span>
				<img src="img/MMMultiply-FOX-plus.jpg">
			</a>
		</div>
	</div>
</section>