<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Return to the Nile Campaign</h1>
			<p>
				In early 2019 National Geographic was launching the premiere of The Fiennes: Return To The Nile. A brilliant documentary series following the legendary explorer Ranulph and his famous actor cousin Joseph retracing an expedition Ran embarked on 50 years ago along the Nile.

			</p>
			<p>
				We were asked to develop concepts for the campaign and once a route was decided, we created the key art’s and also handled the digital campaign build and rollout.
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-ReturnToTheNileCampaign-01.jpg" data-lightbox="RTTN">
						<img src="img/MMMultiply-ReturnToTheNileCampaign-01.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-ReturnToTheNileCampaign-05.jpg" data-lightbox="RTTN">
						<img src="img/MMMultiply-ReturnToTheNileCampaign-05.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-ReturnToTheNileCampaign-06.jpg" data-lightbox="RTTN">
						<img src="img/MMMultiply-ReturnToTheNileCampaign-06.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-ReturnToTheNileCampaign-02.jpg" data-lightbox="RTTN">
						<img src="img/MMMultiply-ReturnToTheNileCampaign-02.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-ReturnToTheNileCampaign-03.jpg" data-lightbox="RTTN">
						<img src="img/MMMultiply-ReturnToTheNileCampaign-03.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-ReturnToTheNileCampaign-04.jpg" data-lightbox="RTTN">
						<img src="img/MMMultiply-ReturnToTheNileCampaign-04.jpg">
					</a>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="wild-launch-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Wild Launch Campaign</span>
				<img src="img/MMMultiply-WildLaunchCampaign-02.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="national-geographic-magazine.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Magazine</span>
				<img src="img/MMMultiply-NatGeo-Mag-10.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State Campaign</span>
				<img src="img/MMMultiply-DeepStateATL-01.jpg">
			</a>			
		</div>

	</div>
</section>