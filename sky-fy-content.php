<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Sky Fy</h1>
			<h4 class="content-filter__sub-title">The Greatest Sci-Fi Movies In The Universe</h4>
					
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">
				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/274914226" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load inview">
					<h3 class="">
						A countdown show that is<br>truly out of this world.
					</h3>
				</div>

				

				<div class="col-12 content__item init-load inview">
					

					<a href="img/MMMultiply-SKY-01.jpg" data-lightbox="sky fy">
						<img src="img/MMMultiply-SKY-01.jpg">
					</a>

				

				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SKY-02.jpg" data-lightbox="sky fy">
						<img src="img/MMMultiply-SKY-02.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SKY-03.jpg" data-lightbox="sky fy">
						<img src="img/MMMultiply-SKY-03.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SKY-FY-Endslate.jpg" data-lightbox="sky fy">
						<img src="img/MMMultiply-SKY-FY-Endslate.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					
					<a href="img/MMMultiply-SKY-FY-LowerThird.jpg" data-lightbox="sky fy">
						<img src="img/MMMultiply-SKY-FY-LowerThird.jpg">
					</a>
	
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SKY-number.jpg" data-lightbox="sky fy">
						<img src="img/MMMultiply-SKY-number.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
				
					<p>
					Sky celebrate the release of Blade Runner 2049 by counting down the 40 greatest sci-fi films ever made. This is the ultimate list chosen by the British public and then picked over and discussed by a host of celebrity sci-fi fans, movie makers, actors and directors.
								
					</p>
					<p>
						We were asked to create a brand and graphics package including number bumpers to help transport the viewer to another dimension.
					</p>
				</div>

			</div>
			
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-sm-9 offset-sm-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="sky-cinema-top-10.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Top 10</span>
				<img src="img/MMMultiply-The-Top-10-Show-04.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="justice-league-uncovered.php" data-pageid="content-page" class="ajax-link">
				<span>Justice League Uncovered</span>
				<img src="img/MMMultiply_SkyCinema_JusticeLeauge_ProfileBatman.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="suicide-squad.php" data-pageid="content-page" class="ajax-link">
				<span>Suicide Squad Packaging</span>
				<img src="img/MMMultiply-SS-Cover.jpg">
			</a>
		</div>
	</div>

</section>