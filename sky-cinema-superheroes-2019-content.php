<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Sky Cinema Superheroes 2019</h1>
			<p>
				Sky approached us to help with the promo for the Superheroes pop up channel on Sky Cinema. The promo was a epic edit combining over 25 heroes from films across the genre and creating the illusion of them all together in the action packed chase sequence.</p>
			<p>
				We were tasked with working on VFX to make the clips work better together and also an awesome end board to tie it all together in a brilliant final scene. The final scene was built entirely in 3D and needed to seamlessly fit in with the rest of the promo.
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/331214205?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/330515019" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/331176474?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="superheroes.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes-09-thumb.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="dc-heroes.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema DC Heroes</span>
				<img src="img/MMMultiply_SkyCinema_DCHeroes_01.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="heroes-and-villains.php" data-pageid="content-page" class="ajax-link">
				<span>Heroes & Villains Promo</span>
				<img src="img/MMMultiply-heroesVillains-02.jpg">
			</a>
		</div>
	</div>
</section>