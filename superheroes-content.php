<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Sky Cinema Superheroes</h1>
			<h4 class='content-filter__sub-title'>A super promo for Sky Cinema’s Superheroes collection.</h4>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/267605107" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>


				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-SuperHeroes-01.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-SkyCinema-SuperHeroes-01.jpg" data-lightbox="superheroes">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>
						We worked closely with the talented team at Sky Creative to create a disruptive promo that would have stand out from the standard slow build movie trailers on Sky Cinema. This execution was a big shift away from how Sky Cinema traditionally promote their content and had to handled in a sensitive way, making sure we stayed aligned with the Sky brand rules and values.
					</p>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-SuperHeroes-04.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-SkyCinema-SuperHeroes-04.jpg" data-lightbox="superheroes">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-SuperHeroes-03.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-SkyCinema-SuperHeroes-03.jpg" data-lightbox="superheroes">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-SuperHeroes-02.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-SkyCinema-SuperHeroes-02.jpg" data-lightbox="superheroes">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-SuperHeroes-05.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-SkyCinema-SuperHeroes-05.jpg" data-lightbox="superheroes">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-SuperHeroes-07.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-SkyCinema-SuperHeroes-07.jpg" data-lightbox="superheroes">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>
						The final promo had real cut through amongst the other promotions and did a fantastic job of grabbing the attention of Superhero fans.
					</p>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-SuperHeroes-08.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-SkyCinema-SuperHeroes-08.jpg" data-lightbox="superheroes">
					</a>
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="sky-cinema-superheroes-2019.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes 2019</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes2019-01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3  projects__single projects__single--thumb  init-load inview">
			<a href="heroes-and-villains.php" data-pageid="content-page" class="ajax-link">
				<span>Heroes & Villains Promo</span>
				<img src="img/MMMultiply-heroesVillains-02.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="ea-fifa19.php" data-pageid="content-page" class="ajax-link">
				<span>EA FIFA 19</span>
				<img src="img/MMMultiply_EA_FIFA19_08.jpg">
			</a>
		</div>
	</div>
</section>