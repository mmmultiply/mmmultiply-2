<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">BBC Lifestyle Rebrand</h1>
			<p>
				We were asked by BBC Worldwide to refresh the identity of their popular Lifestyle channel in South Africa and Poland.
			</p>
			<p>
				The channel was well established in South Africa but the branding and identity needed consideration to be well adapted across the two different cultures. 
			</p>


		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/310375064?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 content__item init-load inview">
					<h3>Entertain. Empower. Inspire.</h3>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-ID01-01.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-ID01-01.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-ID01-02.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-ID01-02.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 push-md-3 content__item init-load inview">
					<p>
						By using a split device across objects and type we could convey the diverse range of lifestyles that the audience can relate to. In addition we created bespoke OSPs for different regions to suit the cultures between South Africa and Poland, setting the tone for the audience and keeping them engaged with the shows that they have tuned in to watch. 

					</p>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-03.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-03.jpg">
					</a>
				</div>



				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-05.gif" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-05.gif">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-06.gif" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-06.gif">
					</a>
				</div>



				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/310373246?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-14.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-14.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-ID03-02.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-ID03-02.jpg">
					</a>
				</div>



				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-12.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-12.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-08.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-08.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-09.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-09.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-02.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-02.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 push-md-3 content__item init-load inview">
					<p>
						The brand proposition was focused around entertaining, inspiring and empowering women. This was an intrinsic part of developing the brand as we wanted the channel to appeal and represent its female audience appropriately, enabling them to identify with the brand.
					</p>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-13.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-13.jpg">
					</a>
				</div>
				
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-04.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-04.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-07.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-07.jpg">
					</a>
				</div>





				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-10.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-10.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-BBCLifestyle-11.jpg" data-lightbox="BBCLifestyle">
						<img src="img/MMMultiply-BBCLifestyle-11.jpg">
					</a>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
				<span>BBC Brit Rebrand</span>
				<img src="img/MMMultiply-BBCBrit-12.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="pick.php" data-pageid="content-page" class="ajax-link">
				<span>Pick Rebrand</span>
				<img src="img/MMMultiply-Pick-01-ID-Popcorn.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single  projects__single--thumb init-load inview">
			<a href="challenge.php" data-pageid="content-page" class="ajax-link">
				<span>Challenge Rebrand</span>
				<img src="img/MMMultiply-Challenge-Modern_1.jpg">
			</a>
		</div>
	</div>
</section>