<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">FOX+ Brand</h1>

			<h4 class="content-filter__sub-title">Identity for FOX’s new on-demand service</h4>

		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">
				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/257115852" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load inview">
					<h3 class="">
						Enter a world of endless entertainment.
					</h3>
				</div>


				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-01.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-01.jpg">
					</a>
				</div>


				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>FOX+ is the new on-demand service from FOX. Offering over 2000 hours of quality content from the huge back catalogue of one of the leading entertainment companies in the world. The best of FOX wherever you are, all in one place.</p>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-02.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-02.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-03.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-03.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-09.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-09.jpg">
					</a>
				</div>
				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-08.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-08.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-04.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-04.jpg">
					</a>
				</div>
				
				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>Working closely with the UK team we created the new identity along with all brand material. We also helped them develop all the templates required for a 360º tiered and phased global marketing campaign.</p>
				</div>

				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-guide-01.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-guide-01.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-guide-02.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-guide-02.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-05.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-05.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-06.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-06.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-Fox-Plus-07.jpg" data-lightbox="fox plus">
						<img src="img/MMMultiply-Fox-Plus-07.jpg">
					</a>
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-3 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
				<span>BBC Brit</span>
				<img src="img/MMMultiply-BBCBrit-01.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single  projects__single--thumb init-load inview">
			<a href="challenge.php" data-pageid="content-page" class="ajax-link">
				<span>Challenge Rebrand</span>
				<img src="img/MMMultiply-Challenge-Modern_1.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="pick.php" data-pageid="content-page" class="ajax-link">
				<span>Pick Rebrand</span>
				<img src="img/MMMultiply-Pick-01-ID-Popcorn.jpg">
				
			</a>
		</div>
	</div>
</section>