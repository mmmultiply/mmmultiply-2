<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">FOX Agent Carter</h1>
			<h4 class='content-filter__sub-title'>We were tasked with creating a premium press pack for FOX’s new Marvel show Agent Carter.
</h4>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_FOX-AgentCarter-4.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply_FOX-AgentCarter-4.jpg" data-lightbox="uktv">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_FOX-AgentCarter-1.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply_FOX-AgentCarter-1.jpg" data-lightbox="uktv">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>Following a graphic novel approach, with bold type, strong colours and premium stock to give the new Marvel acquisition real impact.</p>
				</div>
				
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_FOX-AgentCarter-3.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply_FOX-AgentCarter-3.jpg" data-lightbox="uktv">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_FOX-AgentCarter-2.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply_FOX-AgentCarter-2.jpg" data-lightbox="uktv">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_FOX-AgentCarter-5.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply_FOX-AgentCarter-5.jpg" data-lightbox="uktv">
					</a>
				</div>

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="fox-plus.php" data-pageid="content-page" class="ajax-link">
				<span>FOX+ Brand</span>
				<img src="img/MMMultiply-FOX-plus.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="suicide-squad.php" data-pageid="content-page" class="ajax-link">
				<span>Suicide Squad Packaging</span>
				<img src="img/MMMultiply-SS-Cover.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3  projects__single projects__single--thumb  init-load inview">
			<a href="uktv-pitch.php" data-pageid="content-page" class="ajax-link">
				<span>UKTV Pitch</span>
				<img src="img/MMMultiply-UKTV-Pitch-Pressentation_1_thumb.jpg">
			</a>
		</div>
	</div>
</section>