<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Deep State Title Sequence</h1>
			<h4 class="content-filter__sub-title">Title sequence for FOX's new espionage thriller.</h4>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/263478354" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load inview">
					
					<a href="img/MMMultiply-DeepStateTitles-02.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply-DeepStateTitles-02.jpg">
					</a>
					
				</div>

				<div class="col-12 content__item init-load inview">
					<h3 class="">
						Who's in control?
					</h3>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-07.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-07.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-09.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-09.jpg">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-06.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-06.jpg">
					</a>
				</div>


				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">

						<p>Working closely with FOX UK we created the title treatment and designed and animated the title sequence for the brilliant new espionage thriller Deep State.</p>
				</div>


				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-08.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-08.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-11.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-11.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-12.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-12.jpg">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-03.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-03.jpg">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-10.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-10.jpg">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-13.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-13.jpg">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateTitles-01.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply-DeepStateTitles-01.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">

						<p>Deceit, power and conspiracy drive the new series so we created an introduction that challenges perceptions and obscures reality, slowly revealing the dark secrets that are hidden deep within.
									</p>
				</div>


				<div class="col-12 content__item init-load inview">
					<div class="content__layer-reveal">
					  	<img src="img/MMMultiply-DeepStateTitles-04.jpg">
					  	<div id="video_two">
					  		<img class="content__layer-reveal__top" src="img/MMMultiply-DeepStateTitles-05.jpg">
					  	</div>  
						<div class="content__layer-reveal__handle">
							<i class="arrow left"></i>
							<i class="arrow right"></i>
						</div>		

					</div>
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">


		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State S1 Campaign</span>
				<img src="img/MMMultiply-DeepStateATL-01.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="heroes-and-villains.php" data-pageid="content-page" class="ajax-link">
				<span>Heroes & Villains Promo</span>
				<img src="img/MMMultiply-heroesVillains-02.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="suicide-squad.php" data-pageid="content-page" class="ajax-link">
				<span>Suicide Squad Promo</span>
				<img src="img/MMMultiply-SS-Cover.jpg">
			</a>
		</div>
	</div>
</section>