<?php 
  include 'template-parts/header-html.php';
?>

<body id="projects-page">

<?php 
  include 'template-parts/header.php';
  include 'work-content.php';
  include 'template-parts/footer.php';
?>

</body>

<?php 
  include 'template-parts/footer-html.php';
?>