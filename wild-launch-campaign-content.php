<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">National Geographic Wild Launch Campaign</h1>
			<p>
				At the beginning of 2019 National Geographic Wild was relaunching with a fresh new look and positioning, aligning itself with the parent brand National Geographic.
				We were invited to create the launch campaign showcasing the exciting new brand identity and positioning.
				
			</p>
			<p>
				We created a toolkit with key art, launch promo, teasers and social templates for launch and rollout across the rest of the year with key show premieres or stunts.
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">
				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/330759364" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-08.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-08.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-01.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-01.jpg">
					</a>
				</div>
				
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-07.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-07.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-03.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-03.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-04.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-04.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-09.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-09.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-05.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-05.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-06.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-06.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-10.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-10.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-11.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-11.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-WildLaunchCampaign-02.jpg" data-lightbox="NGWildLaunch">
						<img src="img/MMMultiply-WildLaunchCampaign-02.jpg">
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="national-geographic-magazine.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Magazine</span>
				<img src="img/MMMultiply-NatGeo-Mag-10.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="returntothenile-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Return to the Nile Campaign</span>
				<img src="img/MMMultiply-ReturnToTheNileCampaign-01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="fox-plus.php" data-pageid="content-page" class="ajax-link">
				<span>FOX+ Brand</span>
				<img src="img/MMMultiply-FOX-plus.jpg">
				
			</a>
		</div>
	</div>
</section>