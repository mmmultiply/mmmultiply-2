<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Challenge Rebrand</h1>
			<h4 class='content-filter__sub-title'> Pushing the reach of the channel and brand beyond the confines of the small screen.</h4>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-Challenge-logo.gif" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-logo.gif">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>We were invited by our good friends at Sky to help with the rebrand of their free to air channel, Challenge.</p>		
					
					<p>
						A core brand proposition was required that would cover the channels three core content pillars and expand the reach of the channel and brand beyond the confines of the small screen.
					</p>
				</div>


				<div class="col-12 content__item content__item--img init-load inview">	
					
					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/181187501?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Challenge-Classic_3.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-Classic_3.jpg">
					</a>
				</div>
				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Challenge-Classic_4.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-Classic_4.jpg">
					</a>
				</div>
				
				

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/181187923?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>
				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Challenge-Physical_3.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-Physical_3.jpg">
					</a>
				</div>
				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Challenge-Physical_5.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-Physical_5.jpg">
					</a>
				</div>



				
				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/181187688?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>
				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Challenge-Modern_3.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-Modern_3.jpg">
					</a>
				</div>
				<div class="col-12 col-md-6 content__item init-load inview">
					<a href="img/MMMultiply-Challenge-Modern_4.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-Modern_4.jpg">
					</a>
				</div>




				<div class="col-12 content__item init-load inview">

					<a href="img/MMMultiply-Challenge-menu-03.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-menu-03.jpg">
					</a>
					
				</div>

				<div class="col-12 col-md-6 content__item content__item--img init-load inview mb-0">
					<a href="img/MMMultiply-Challenge-menu-01.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-menu-01.jpg">
					</a>
				</div>
				<div class="col-12 col-md-6 content__item content__item--img init-load inview mb-0">
					<a href="img/MMMultiply-Challenge-menu-02.jpg" data-lightbox="Challenge">
						<img src="img/MMMultiply-Challenge-menu-02.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">


						<p>By working closely with the in-house creative team and other key stake holders, we were able to explore what the main elements of the brand were.</p>

						<p>'Challenge Accepted' was our collective reply - the war cry of the Challenge audience, a positive call to arms bringing the audience into the game. We wanted to bring in elements of interactivity with the viewer wherever possible and await their reply, 'Challenge Accepted'.</p>	

					
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-3 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
				<span>BBC Brit Rebrand</span>
				<img src="img/MMMultiply-BBCBrit-01.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="fox-plus.php" data-pageid="content-page" class="ajax-link">
				<span>FOX+ Brand</span>
				<img src="img/MMMultiply-FOX-plus.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="pick.php" data-pageid="content-page" class="ajax-link">
				<span>Pick Rebrand</span>
				<img src="img/MMMultiply-Pick-01-ID-Popcorn.jpg">
				
			</a>
		</div>
	</div>
</section>