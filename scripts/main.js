var app = {};
var e = {};
var g = {};

(function(w, d){
    var b = d.getElementsByTagName('body')[0];
    var s = d.createElement("script"); 
    var v = !("IntersectionObserver" in w) ? "8.15.0" : "10.15.0";
    s.async = true; // This includes the script as async. See the "recipes" section for more information about async loading of LazyLoad.
    s.src = "https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/" + v + "/lazyload.min.js";
    w.lazyLoadOptions = {/* Your options here */};
    b.appendChild(s);
}(window, document));

e.progress = $('.ajax-progress');

app.refreshPageObjs = function() {
    e.meetTheTeam = $(".meet-the-team");
    e.homeV6 = $('#index-content');
    e.work = $('#projects-page');
    e.content = $('#content-page');
    e.studio = $('#studio-page');
    e.about = $('#about-page');

}

app.refreshPageObjs();

app.windowVars = function() {
    windowHeight = $(window).height();
    windowWidth = $(window).width();
    docHeight = $( "body" ).height() - $( window ).height();
}

app.runSite = function(){

    app.refreshPageObjs();
    app.windowVars();


	window.onload = function() {
        // app.introCanvas();
        app.generalFunctions();
        app.pageContentGeneral();

		$(".loader").fadeOut(200);

	    // app.findPageScripts();

	};

	$( document ).ready(function() {

		// console.log('run app run!');

	    if ( e.meetTheTeam[length] != undefined ) {
	    	app.meetTheTeam();
	    }

	});

}

app.runSite();

app.findPageScripts = function() {

    app.refreshPageObjs();

    app.ajaxScript();

    // console.log('find script');
    // console.log('body is '+ $('body').attr('id'));

    var body = $('body');

    if ( body.is("#index-content") ) {
        // console.log('run home');
        app.homeV6();   
    }

    if ( body.is("#projects-page") ) {
        // console.log('run work');
        app.work();    
    }

    if (body.is("#content-page")) {
        // console.log('run content');
        app.contentPage();
    }

    if (body.is("#studio-page")) {
        // console.log('run studio');
        app.studio();
    }

    if (body.is("#about-page")) {
        // console.log('run about');       
        app.about();
    }

    app.staggerLoad( 100 ); 
    
}


//global functions

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


app.staggerLoad = function( time ) {

	$.each($('.load'), function(i, el){

	    setTimeout(function(){

	       $(el).removeClass('load');

	    },time + ( i * time ));

	});
}

var docHeight,
    windowHeight,
    windowWidth;

// add delay capabilities to event handlers https://github.com/yckart/jquery.unevent.js
;(function ($) {
    var on = $.fn.on, timer;
    $.fn.on = function () {
        var args = Array.apply(null, arguments);
        var last = args[args.length - 1];

        if (isNaN(last) || (last === 1 && args.pop())) return on.apply(this, args);

        var delay = args.pop();
        var fn = args.pop();

        args.push(function () {
            var self = this, params = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                fn.apply(self, params);
            }, delay);
        });

        return on.apply(this, args);
    };
}(this.jQuery || this.Zepto));


  /* Pause videos when out of view */
  inView('video')
    .on('enter', function(el){ el.play(); })
    .on('exit', function(el) { el.pause(); });


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function is_touch_device() {
  var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  var mq = function(query) {
    return window.matchMedia(query).matches;
  }

  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
    return true;
  }

  // include the 'heartz' as a way to have a non matching MQ to help terminate the join
  // https://git.io/vznFH
  var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
  return mq(query);
}


// throttle function for scroll
function throttle(fn, wait) {
  var time = Date.now();
  return function() {
    if ((time + wait - Date.now()) < 0) {
      fn();
      time = Date.now();
    }
  }
}