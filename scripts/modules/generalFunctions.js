app.generalFunctions = function() {
	
	// console.log('general functions triggered');

	// event listeners
	$(".contact-link").hover( 

		function() {
			if (!$('html').hasClass('touchevents')) {
				$(this).addClass('active').find('.link-navigation').html("Close");
				$(".contact-info").addClass("active");
			}
		},
		function() {
			
		}
	)

	$(".contact-info").hover( 
		function() {},
		function() { 

			if (!$('html').hasClass('touchevents')) {

				$(this).removeClass('active');
				$(".contact-link").removeClass('active').find('.link-navigation').html("Contact");
			}
		}
	)

	$(".contact-link").click( function() {

		console.log('contact-link click');

		$(this).toggleClass('active');
		$(this).find('.link-navigation').html("Close");
		$(".contact-info").toggleClass("active");
		
		if (!$(this).hasClass('active')) {
			$(this).find('.link-navigation').html("Contact");

		}

	})

	// mobile burger menu
	$(".mobile-icon").on('touch click', function() {
		// alert('meni icon touched!');
		$('.header__links').toggleClass("open");
	});




	function SetCookie(c_name,value,expiredays) {
		var exdate=new Date()
		exdate.setDate(exdate.getDate()+expiredays)
		document.cookie=c_name+ "=" +escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toUTCString())
	}

	if( document.cookie.indexOf("eucookie") ===-1 ){
		$("#cookieConsent").css('opacity','1');
		SetCookie('eucookie','eucookie',365*10)
	}
	else {
		$("#cookieConsent").remove();
	}

	$('.cookieConsentOK').on('click touch', function () {
		$("#cookieConsent").remove();
		
	});

}