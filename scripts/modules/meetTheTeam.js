app.meetTheTeam = function() {

	var container;
	var camera, scene, renderer, composer;
	var glitchPass;

	var mouseX = 0, mouseY = 0;
	var windowHalfX = window.innerWidth / 2;
	var windowHalfY = window.innerHeight / 2;
	init();
	animate();
	var head;


	function init() {

		container = $(".meet-the-team");
		console.log(container);
		// container = document.createElement( 'div' );
		// document.body.appendChild( container );
		camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
		camera.position.z = 250;
		// scene
		scene = new THREE.Scene();
		// scene.background = new THREE.Color( 0xff0000, 0);

		var ambientLight = new THREE.AmbientLight( 0xcccccc, 10.4 );
		scene.add( ambientLight );
		var pointLight = new THREE.PointLight( 0xffffff, 2.8 );
		camera.add( pointLight );
		scene.add( camera );
		// texture
		var manager = new THREE.LoadingManager();
		manager.onProgress = function ( item, loaded, total ) {
			console.log( item, loaded, total );
		};

		var textureCube = new THREE.CubeTextureLoader()
			.setPath( 'img/cube/')
			.load( [ 'dark-s_px.jpg', 'dark-s_nx.jpg', 'dark-s_py.jpg', 'dark-s_ny.jpg', 'dark-s_pz.jpg', 'dark-s_nz.jpg' ] );

		var textureLoader = new THREE.TextureLoader( manager );
		// var texture = textureLoader.load( 'img/texture.jpg' );

		var texture = new THREE.MeshLambertMaterial( { color: 0xffffff, envMap: textureCube, refractionRatio: 0.5  } );

		// var texture = new THREE.MeshToonMaterial( {
		// // 	map: "",
		// // 	bumpMap: imgTexture,
		// // 	bumpScale: bumpScale,
		// 	color: 0xffee00,
		// 	specular: 0xffffff,
		// 	reflectivity:0.8,
		// 	shininess: 1,
		// 	envMap: textureCube
		// } );


		// model
		var onProgress = function ( xhr ) {
			if ( xhr.lengthComputable ) {
				var percentComplete = xhr.loaded / xhr.total * 100;
				console.log( Math.round(percentComplete, 2) + '% downloaded' );
			}
		};
		var onError = function ( xhr ) {
		};


		var objLoader = new THREE.OBJLoader( manager );

		objLoader.setPath( 'img/' );
		objLoader.load( 'obj.obj', function ( object ) {
			// console.log(object);

			head = object.children[0];
			head.scale.set(0.05, 0.05, 0.05);

			head.position.y = -50;
			head.material = texture;
			head.material.side = THREE.DoubleSide;

			scene.add( head );

			// console.log(head);
		} );

		// var loader = new THREE.OBJLoader( manager );
		// loader.load( 'img/obj.obj', function ( object ) {
		// 	object.traverse( function ( child ) {
		// 		if ( child instanceof THREE.Mesh ) {
		// 			child.material.map = texture;
		// 		}
		// 	} );
		// 	object.position.y = - 95;
		// 	scene.add( object );
		// }, onProgress, onError );
		//



		renderer = new THREE.WebGLRenderer( {alpha: true });
		renderer.setPixelRatio( window.devicePixelRatio );
		renderer.setSize( window.innerWidth, window.innerHeight );
		container.append( renderer.domElement );



		// postprocessing
		composer = new THREE.EffectComposer( renderer );
		composer.addPass( new THREE.RenderPass( scene, camera ) );
		glitchPass = new THREE.GlitchPass();
		glitchPass.renderToScreen = true;
		composer.addPass( glitchPass );



		document.addEventListener( 'mousemove', onDocumentMouseMove, false );
		//
		window.addEventListener( 'resize', onWindowResize, false );
	}
	function onWindowResize() {
		windowHalfX = window.innerWidth / 2;
		windowHalfY = window.innerHeight / 2;
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( window.innerWidth, window.innerHeight );
		composer.setSize( window.innerWidth, window.innerHeight );
	}
	function onDocumentMouseMove( event ) {
		mouseX = ( event.clientX - windowHalfX ) / 2;
		mouseY = ( event.clientY - windowHalfY ) / 2;
	}
	//
	function animate() {
		requestAnimationFrame( animate );
		render();
	}
	function render() {
		if (head) {
			head.rotation.y += 0.005;
		}
		camera.position.x += ( mouseX - camera.position.x ) * .05;
		camera.position.y += ( - mouseY - camera.position.y ) * .05;
		camera.lookAt( scene.position );

		composer.render();
		// renderer.render( scene, camera );
	}
	

};