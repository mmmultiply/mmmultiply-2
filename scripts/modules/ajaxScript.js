app.ajaxScript = function() {

	// console.log('run ajax');

    var ajaxLinkElem;
    var newPageId;

	$('.ajax-link').unbind().click(function (event) {

        // console.log('ajax-click');
		event.preventDefault();

        ajaxLinkElem = $(this);
        newPageId = $( this ).attr('data-pageid');

        $(".fake-logo").css('opacity', '0');

        //////////////////////////////////////////////
        //-------content/project filter code--------//
        //////////////////////////////////////////////


        // animate project filter on work page
        var projectFilter = $('.project-filter');
        var bgCross = $('.bg-cross');

        if (bgCross.length > 0) {
            $(bgCross).css('filter','blur(5px)').animate({
                opacity:0,
            }, {
                duration: 500
            })
        }

        if (projectFilter.length > 0 ) {
            // console.log('content filter is on the page');

            var filterChildren = $(projectFilter).children();

            $(filterChildren).each(function(i) {
                delay =(i)*50;
                $(this).delay(delay).animate({
                   opacity:0,
                   marginLeft:"-50px"
                }, {
                duration: 150,
                complete: function() {
                       // $(this).addClass('animated slideInLeft');
                    }
                });  
            });
        }



        //////////////////////////////////////////////
        //-------the rest of the ajax stuff---------//
        //////////////////////////////////////////////
        ajaxLoad(ajaxLinkElem);
        
    });


    function ajaxLoad(elem) {
        // console.log('ajax load');

        //remove '-content' from url for push state.
        var defaultUrl = elem.attr('href');
        // console.log('defaultUrl before replace ='+ defaultUrl);

        var correctUrl = defaultUrl.replace('.php', '');
        correctUrl += "-content.php";

        // console.log('correctUrl after replace ='+ correctUrl);


        $.ajax({
            url: correctUrl,
            type: 'GET',

            beforeSend: function() {
                TweenMax.fromTo('.ajax-progress', 1, {width:'0%', opacity:1}, {width:'80%', opacity:1});
                // e.progress.removeClass('success').addClass('start');
            },

            success: function (data) {

                // e.progress.removeClass('start').addClass('success');

                TweenMax.to('.ajax-progress', 0.2, {width:'100%'});
                TweenMax.to('.ajax-progress', 0.2, {opacity:0, delay:0.2});


                // console.log('success');

                var curWidth = $( window ).width();

                TweenMax.to(".ajax", 1.3, { x:-curWidth, ease:Power2.easeIn, delay: 0.3, onComplete: function() {

                        $('body').removeAttr('id').attr('id', newPageId);

                        $( '.ajax' ).remove();

                        $('header').after(data);

                        $('.ajax').addClass('inview');
                        $(".init-load").addClass("load").removeClass("init-load");

                        history.pushState(null, null, defaultUrl );

                        app.windowVars();

                        app.pageContentGeneral();
                        // app.findPageScripts(); << this content specific function is run inside page content general after the images have loaded.
                        
                        app.staggerLoad( 100 );   
                        window.scrollTo(0,0);
                        app.windowVars();
                    }
                })
                 
            },
            error: function () {
                // console.log('error');
            },
            complete: function() {

                // console.log('complete');

                $( window ).off();

                $(".contact-link, .contact-info").removeClass('active');
                $(".contact-link.link-navigation").html("Close");
                
            }
        });

       
    }

    window.addEventListener('popstate', function(e) {
        location.reload();
    });

    window.addEventListener('pushstate', function(e) {
        location.reload();
    });
}