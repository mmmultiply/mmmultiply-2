app.studio = function() {
	
	$('.header__links').fadeIn('fast');

	var labelContainer = $('.studio-info-nav');

	var PSV = PhotoSphereViewer({
		container: 'photosphere',
		panorama: 'img/studio-front.jpg',
		navbar: true,
		time_anim: false,
		mousewheel: false,
		gyroscope: true,
		fisheye: true,
		navbar: false,
		anim_speed: '1.2rpm',
		time_anim: 0,
		markers: [
			{
				id: 'studio',
				// ellipse: [50,70],
				image: 'img/arrow-forwards.svg',
				width:100,
				height:100,
				x: 5031,
				y: 1896,
				tooltip: 'Studio Back'
			}
			// {
			// 	id: 'foose',
			// 	polygon_px: [
			// 	[4245, 1729], [4205, 1808],[4345, 2121], [4590, 2139],
			// 	[5087, 1850], [5118, 1742], [5115, 1684], [4813, 1620]
			// 	],
			// 	svgStyle: {
			// 	fill: 'rgba(200, 0, 0, 0.0)',
			// 	stroke: 'rgba(200, 0, 50, 0.8)',
			// 	strokeWidth: '2px'
			// 	},
			// 	tooltip: {
			// 	content: 'FOOSEBALL!',
			// 	position: 'right bottom'
			// 	}	
			// },
			// { 
			// 	// image marker that opens the panel when clicked
			// 	id: 'concept',
			// 	x: 2615,
			// 	y: 1428,
			// 	// image: 'img/marker-concept.svg',
			// 	html: '<div class="label-wrapper"><span class="label-number">1.1</span><span class="label-break"></span><span class="label-title">Concept</span></div>',
			// 	anchor: 'bottom center'
			// },

			// { 
			// 	// image marker that opens the panel when clicked
			//      id: 'planning',
			//      x: 3723,
			//      y: 1398,
			//      // image: 'img/marker-planning.svg',
			//      html: '<div class="label-wrapper"><span class="label-number">1.2</span><span class="label-break"></span><span class="label-title">Planning</span></div>',
			//      // width: 300,
			//      // height: 100,
			//      anchor: 'bottom center'
			// },

			// {
			// 	id: 'bundesliga',
			// 	x: 4735,
			// 	y: 1850,
			// 	// image: 'img/marker-planning.svg',
			// 	html: '<div class="label-extra-info"><div class="label-extra-info__pulse"></div></div>',
			// 	anchor: 'bottom center',
			// 	tooltip: 'Bundesliga'
			// }
		]
	});

	PSV.on('select-marker', function(marker, dblclick) {

		changeLabelNavClass(marker.id);

		console.log(marker);
		if (marker.id == "studio") {
			PSV.clearMarkers();
			PSV.setPanorama('img/studio-back.jpg');
			setupBack();
		}

		if (marker.id == "sofas") {
			PSV.clearMarkers();
			PSV.setPanorama('img/studio-front.jpg');
			setupFrontNav();
			// setupFoose();
		}

		// if (marker.id == "foose") {
		// 	alert('you clicked on the fooseball table');
		// }

		// if (marker.id == "concept") {
		// 	$("#concept").css('display', 'block').css('visibility', 'visible');
  //           TweenMax.to('#concept', 0.2, {opacity:'1'});
		// }

		// if (marker.id == "planning") {
		// 	$("#planning").css('display', 'block').css('visibility', 'visible');
  //           TweenMax.to('#planning', 0.2, {opacity:'1'});
		// }

		// if (marker.id == "design") {
		// 	$("#design").css('display', 'block').css('visibility', 'visible');
  //           TweenMax.to('#design', 0.2, {opacity:'1'});
		// }

		// if (marker.id == "vfx") {
		// 	$("#vfx").css('display', 'block').css('visibility', 'visible');
  //           TweenMax.to('#vfx', 0.2, {opacity:'1'});
		// }	

		// if (marker.id == "bundesliga") {
		// 	$("#bundesliga").css('display', 'block').css('visibility', 'visible');
  //           TweenMax.to('#bundesliga', 0.2, {opacity:'1'});
		// }	
		// if (marker.id == "bbc") {
		// 	$("#bbc").css('display', 'block').css('visibility', 'visible');
  //           TweenMax.to('#bbc', 0.2, {opacity:'1'});
		// }	
		// if (marker.id == "ea") {
		// 	$("#ea").css('display', 'block').css('visibility', 'visible');
  //           TweenMax.to('#ea', 0.2, {opacity:'1'});
		// }	
	});

	$(".close").click( function() {
		TweenMax.to(".studio-info", .3, {autoAlpha: 0});
		labelContainer.removeClass("sofas planning concept studio design vfx");
	});

	function setupFrontNav() {
		PSV.addMarker(
			{
				id: 'studio',
				// ellipse: [50,70],
				image: 'img/arrow-forwards.svg',
				width:100, //WAS 150
				height:100,
				x: 5031,
				y: 1896,
				tooltip: 'Studio Back'
			}

		);

		// PSV.addMarker({
		// 	// polygon marker
		// 	id: 'foose',
		// 	polygon_px: [
		// 	[4245, 1729], [4205, 1808],[4345, 2121], [4590, 2139],
		// 	[5087, 1850], [5118, 1742], [5115, 1684], [4813, 1620]
		// 	],
		// 	svgStyle: {
		// 	fill: 'rgba(200, 0, 0, 0)',
		// 	stroke: 'rgba(200, 0, 50, 0.8)',
		// 	strokeWidth: '2px'
		// 	},
		// 	tooltip: {
		// 	content: 'FOOSEBALL!',
		// 	position: 'right bottom'
		// 	}
		// });

		// PSV.addMarker({
		// 	id: 'concept',
		// 	x: 2615,
		// 	y: 1428,
		// 	// image: 'img/marker-concept.svg',
		// 	html: '<div class="label-wrapper"><span class="label-number">1.1</span><span class="label-break"></span><span class="label-title">Concept</span></div>',
		// 	anchor: 'bottom center'
		// });

		// PSV.addMarker({
		// 	id: 'planning',
		// 	x: 3723,
		// 	y: 1398,
		// 	html: '<div class="label-wrapper"><span class="label-number">1.2</span><span class="label-break"></span><span class="label-title">Planning</span></div>',
		// 	anchor: 'bottom center'
		// });


		// PSV.addMarker({
		// 	id: 'bundesliga',
		// 	x: 4735,
		// 	y: 1850,
		// 	html: '<div class="label-extra-info"><div class="label-extra-info__pulse"></div></div>',
		// 	anchor: 'bottom center',
		// 	tooltip: 'Bundesliga'
		// });

	}

		

	function setupBack() {
		PSV.addMarker({
			id: 'sofas',
			image: 'img/arrow-forwards.svg',
			width:100,
			height:100,
			x: 2751,
			y: 1732,
			tooltip: 'Studio Front'
		});

		// PSV.addMarker({ 
		// 	// image marker that opens the panel when clicked
		//     id: 'design',
		//     x: 3787,
		//     y: 1400,
		//      // image: 'img/marker-design.svg',
		//     html: '<div class="label-wrapper"><span class="label-number">2.1</span><span class="label-break"></span><span class="label-title">Design</span></div>',
		//     anchor: 'bottom center'

		// });

		// PSV.addMarker({ 
		// 	// image marker that opens the panel when clicked
		//      id: 'vfx',
		//      x: 4763,
		//      y: 1388,
		//      // image: 'img/marker-vfx.svg',
		// 	 html: '<div class="label-wrapper"><span class="label-number">2.2</span><span class="label-break"></span><span class="label-title">3D &amp; VFX</span></div>',
		//      anchor: 'bottom center'
		// });

		// PSV.addMarker({
		// 	id: 'bbc',
		// 	x: 4707,
		// 	y: 1723,
		// 	html: '<div class="label-extra-info"><div class="label-extra-info__pulse"></div></div>',
		// 	anchor: 'center center',
		// 	tooltip: 'BBC Brit'
		// });

		// PSV.addMarker({
		// 	id: 'ea',
		// 	x: 4999,
		// 	y: 1716,
		// 	html: '<div class="label-extra-info"><div class="label-extra-info__pulse"></div></div>',
		// 	anchor: 'center center',
		// 	tooltip: 'FIFA 2019'
		// });


	}





	// ————————————————————————————————— //
	// ––––––SETUP NAVIGATION TABS–––––– //
	// ————————————————————————————————— //



	$('.studio-info-nav__label').click( function() {

		var labelName = $(this).attr('label');
		console.log(labelName);
		changeLabelNavClass(labelName);


		if (labelName == "sofas" ) {

			TweenMax.to(".studio-info", .3, {autoAlpha: 0});

			PSV.clearMarkers();
			PSV.setPanorama('img/studio-front.jpg');
			setupFrontNav();

		}
		// else if (labelName == "concept" ) {			

		// 	TweenMax.to(".studio-info", .3, {autoAlpha: 0, onComplete: 
		// 		function() {
		// 			$("#concept").css('display', 'block').css('visibility', 'visible');
		// 			TweenMax.to('#concept', 0.2, {opacity:'1'})
		// 		}
		// 	});


		// }
		// else if (labelName == "planning" ) {

		// 	TweenMax.to(".studio-info", .3, {autoAlpha: 0, onComplete: 
		// 		function() {
		// 			$("#planning").css('display', 'block').css('visibility', 'visible');
		// 			TweenMax.to('#planning', 0.2, {opacity:'1'})
		// 		}
		// 	});

		// }
		else if (labelName == "studio" ) {
			TweenMax.to(".studio-info", .3, {autoAlpha: 0});
			PSV.clearMarkers();
			PSV.setPanorama('img/studio-back.jpg');
			setupBack();

		}
		// else if (labelName == "design" ) {

		// 	TweenMax.to(".studio-info", .3, {autoAlpha: 0, onComplete: 
		// 		function() {
		// 			$("#design").css('display', 'block').css('visibility', 'visible');
		// 			TweenMax.to('#design', 0.2, {opacity:'1'})
		// 		}
		// 	});

		// }
		// else if (labelName == "vfx" ) {

		// 	TweenMax.to(".studio-info", .3, {autoAlpha: 0, onComplete: 
		// 		function() {
		// 			$("#vfx").css('display', 'block').css('visibility', 'visible');
		// 			TweenMax.to('#vfx', 0.2, {opacity:'1'})
		// 		}
		// 	});

		// }
		// else if (labelName == "bundesliga" ) {

		// 	TweenMax.to(".studio-info", .3, {autoAlpha: 0, onComplete: 
		// 		function() {
		// 			$("#bundesliga").css('display', 'block').css('visibility', 'visible');
		// 			TweenMax.to('#bundesliga', 0.2, {opacity:'1'})
		// 		}
		// 	});

		// }
		// else if (labelName == "ea" ) {

		// 	TweenMax.to(".studio-info", .3, {autoAlpha: 0, onComplete: 
		// 		function() {
		// 			$("#bundesliga").css('display', 'block').css('visibility', 'visible');
		// 			TweenMax.to('#ea', 0.2, {opacity:'1'})
		// 		}
		// 	});

		// }

		// else if (labelName == "bbc" ) {

		// 	TweenMax.to(".studio-info", .3, {autoAlpha: 0, onComplete: 
		// 		function() {
		// 			$("#bundesliga").css('display', 'block').css('visibility', 'visible');
		// 			TweenMax.to('#bbcs', 0.2, {opacity:'1'})
		// 		}
		// 	});

		// }	

	});

	function changeLabelNavClass(x) {
		labelContainer.removeClass("sofas planning concept studio design vfx bbc ea").addClass(x);
	}

};