app.pageContentGeneral = function() {
	//this file should be triggered at the begining of every page load

	$('.header__links').removeClass("open");
	$('.header__links').fadeIn('fast');

	var lazyLoad = new LazyLoad({
	    elements_selector: ".lazyload"
	});

	$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function(event) {
	// On-page links
		if (
		  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
		  && 
		  location.hostname == this.hostname
		) {
		  // Figure out element to scroll to
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		  // Does a scroll target exist?
		  if (target.length) {
		    // Only prevent default if animation is actually gonna happen
		    event.preventDefault();
		    $('html, body').animate({
		      scrollTop: target.offset().top
		    }, 1000, function() {
		      // Callback after animation
		      // Must change focus!
		      var $target = $(target);
		      $target.focus();
		      if ($target.is(":focus")) { // Checking if the target was focused
		        return false;
		      } else {
		        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
		        $target.focus(); // Set focus again
		      };
		    });
		  }
		}
	});


	//can videos play?
	$.each( $('video'), function(i,elem) {
	    elem.oncanplay = function() {
	        
	       // console.log('can play');

	       $(this).parent().removeClass('no-play'); 

	    } 

	    if ($(this).parent().hasClass('can-play') == false) {
	        // console.log('can not play');
	        $(this).parent().addClass('no-play'); 
	    }
	});


	//setup load classes;
	$(".init-load").addClass("load").removeClass("init-load");


	// $('.inview').imagesLoaded().progress( function( instance, image ) {
	// 	var result = image.isLoaded ? 'loaded' : 'broken';
	// 	if (result === 'loaded') {
	// 		inView(this)
	// 			.on('enter', function(el){ this.removeClass('inview') })
	//   			.on('exit', function(el) { this.addClass('inview'); });
	// 	}
	// });

	inView('.inview')
	.on('enter', function(el){ 
		// console.log('inview');
		$(el).imagesLoaded().progress( function( instance, image ) {
		    var result = image.isLoaded ? 'loaded' : 'broken';
		    // console.log( 'image is ' + result + ' for ' + image.img.src );

		    if (result == 'loaded') {
		    	$(el).removeClass('inview');
		    } else if (result == 'broken') {
		    	//do nothing
		    } else if (result != 'loaded') {
		    	//is not loaded or broken, just show it
		    	$(el).removeClass('inview');
		    }
		});

		if ($(el).find('iframe') != "") {
			$(el).removeClass('inview');
		}
	})
	.on('exit', function(el) { 
		$(el).addClass('inview'); 
	});

	$('body').imagesLoaded( function() {
	 	app.windowVars();
	 	app.findPageScripts();
	});

}
 