app.about = function() {

	var windowWidth = $(window).width();
	var windowHeight = $(window).height();

	
	// console.log(viewer);


	// var cursors = e.about.find('.collab--cursor');
	// console.log('colab module');

	var	leftPos = [];
	var	topPos = [];
	// setCursorPos();

	var gammaRotation;
	var betaRotation;

	
	

	// function setCursorPos(i, elem) {
	// 	var windowWidth = $(window).width();
	// 	leftPos[i] = ((windowWidth/3)*Math.random()) - (windowWidth/6);
	// 	topPos[i] = (($('.collab-module').height()/3)*Math.random());
	// 	// console.log('set cursor pos');
	// 	// console.log('topPos'+ i + '=' +topPos[i]);

	// }

	var tl = new TimelineLite,
	    mySplitText = new SplitText(".collab-message", {type:"words,chars"});
	    words = mySplitText.words; //an array of all the divs that wrap each character
	    // console.log(words);
    	tl.staggerFromTo(words, 0.5, {opacity:0}, {opacity:1}, 0.2, "+=0.1");
	    

	// TweenLite.set("#quote", {perspective:400});

	$('.collab-message').css('opacity', '1');


	inView('.collab-module')
    .on('enter', function(el){ 
    	$(el).addClass('active');
    	// $('.header__links').fadeIn('fast');

    	// tl = new TimelineLite,
	    // mySplitText = new SplitText(".collab-message", {type:"words,chars"});
	    // words = mySplitText.words; //an array of all the divs that wrap each character

    	// tl.staggerFromTo(words, 0.5, {opacity:0}, {opacity:1}, 0.2, "+=0.1");
    })
    .on('exit', function(el) { 
    	$(el).removeClass('active') 
    	// $('.header__links').fadeOut('fast');

    });

    inView('.skills')
    .on('enter', function(el){ $(el).addClass('active') })
    .on('exit', function(el) { $(el).removeClass('active') });

	inView('.img-grid')
    .on('enter', function(el){ 
    	$(el).addClass('active'); 
	    if (windowWidth > 575) {
			$(window).on( 'scroll', throttle(imgGridHandler, 50) );

	    }



	})
    .on('exit', function(el) { 
    	$(el).removeClass('active');
    });


	// FADE DOWN NAV ON SCROLL
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('header').outerHeight();

	$(window).scroll(function(event){
	    didScroll = true;
	});

	setInterval(function() {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 250);

	function hasScrolled() {
	    var st = $(this).scrollTop();

		if ( $(window).width() > 992) {  

		    // Make sure they scroll more than delta
		    if(Math.abs(lastScrollTop - st) <= delta)
		    	return;
	    
		    // If they scrolled down and are past the navbar, add class .nav-up.
		    // This is necessary so you never see what is "behind" the navbar.
		    if (st > lastScrollTop && st > navbarHeight){
		        // Scroll Down
		        $('.header__links').fadeOut('fast');
		    } else {
		        // Scroll Up
		        if(st + $(window).height() < $(document).height()) {
		           $('.header__links').fadeIn('fast');
		        }
		    }
	    
	    	lastScrollTop = st;
			}
	    
	    }

    var imgGridRight = $('.img-grid__right');
    var imgGridLeft = $('.img-grid__left');

    function imgGridHandler() {
    	
    	// console.log('scrolled');

    	var scrollTop = $(window).scrollTop(),

    		leftElementOffset = $(imgGridLeft).offset().top,
    	    leftDistance      = ((leftElementOffset + $(imgGridLeft).height()) - scrollTop),
    	    rightElementOffset = $(imgGridRight).offset().top,
    	    rightDistance      = (rightElementOffset - scrollTop);

    	var offset = -40;

    	var rightPos = offset * (rightDistance/windowHeight) ;
    	$(imgGridRight).css('margin-left', rightPos+'%');

    	var leftPos = offset - (offset * (leftDistance/windowHeight)) ;
    	$(imgGridLeft).css('margin-left', leftPos+'%');

    }


    // $.each(cursors, function(i, elem) {
    // 	setCursorPos(i, elem);

    // 	$(elem).css('left', leftPos[i]+(windowWidth/2)+'px');
    // 	$(elem).css('top', topPos[i]+($('.collab-module').height()/6)+'px');
    // });

    var mousePcX,
    	mousePcY;
	//is not touch
	if (!is_touch_device()) {


		windowWidth = $(window).width();


	    $( ".collab-module" ).hover(
	    function() {

	    	// console.log('collab-module hover');

      	    $( ".collab-module" ).mousemove(function( event ) {

		    	// console.log('collab-module mouseOver');


      			// $.each(cursors, function(i, elem) {
      			//   	var newLeftPos = [];
      			//   	var newTopPos = [];

      			//   	newLeftPos[i] = leftPos[i] + event.pageX;
      			//   	newTopPos[i] = topPos[i] + event.pageY;

      			//   	//check if out of bounds along x axis
      			//   	if (newLeftPos[i] > $('.collab-module').width() || newLeftPos[i] < 0) {
      			//   		newLeftPos[i] = leftPos[i]+($('.collab-module').width()/2);
      			//   	}
      			  	
      			//   	//check if out of bounds along y axis
      			//   	if (newTopPos[i] > $('.collab-module').height() || newTopPos[i] < 0) {
      			//   		newTopPos[i] = topPos[i]+($('.collab-module').height()/6);
      			//   	}

      			//   	//apply new css
      			//   	$(elem).css('left', newLeftPos[i]+'px').css('top', newTopPos[i]+'px');

      			// })


      			// //get % from left

      			mousePcX = event.pageX/$(window).width();
      			mousePcY = event.pageY/$(window).height();



      		});

	    }, function() {

	    	mousePcX = 0.5;
	    	mousePcY = 0.5;


	   //    	$.each(cursors, function(i, elem) {  	  

	   //    	  	$(elem).css('left', leftPos[i]+(windowWidth/2)+'px');
				// $(elem).css('top', topPos[i]+($('.collab-module').height()/6)+'px');

	   //    	})

	    });

	} 


	window.addEventListener( 'deviceorientation', onOrietationChange, false );


	function onOrietationChange(e) {

		// gammaRotation = e.gamma ? e.gamma * (Math.PI / 500) : 0;
		// betaRotation = e.beta ? e.beta * (Math.PI / 500) : 0;

		var x = e.beta;  // In degree in the range [-180,180]
		var y = e.gamma; // In degree in the range [-90,90]

		// if (x > 90) { 
		// 	if (y > 0) { y = 90};
		// 	if (y < 0) { y = -90};
		// 	// y = y*-1
		// 	// alert('woah steady on');
		// };

		y += 90;


		gammaRotation=(y/180)-0.5




		if (x >  90) { x =  90};
		if (x < 0) { x = 0};

		betaRotation=(x/90)-0.5; //get percentage of rotation about x axis where 0 = 0 & 90 = 1

		
	
	}

	// var distToMoveHeadline = $('.headline__animation h4').width()+ 40;
	// TweenMax.to('.headline__animation', 6, {ease: Power0.easeNone, x:-distToMoveHeadline, repeat: -1});


	//skills
	TweenMax.to( ".services__content  .branding",0.1, {autoAlpha:1});
	$('.services__images .branding').addClass('active');


	$('.services__list li').on('click tap', function(){
		var theSkill = $(this).attr("class");
		var clickedElem = $(this);
		console.log(theSkill);

		$('.services__list li').removeClass('active');
		$('.services__images div').removeClass('active');

		clickedElem.addClass('active');

		TweenMax.to( ".services__content p",0.3, {
			autoAlpha:0,
			onComplete: function() {
				TweenMax.to( ".services__content ."+theSkill ,0.3, {autoAlpha:1});
				
				$('.services__images .' + theSkill).addClass('active');

			}
		});

	}); 







	//canvas stuff

	var newWidth = $('.collab-module').width(),
		newHeight = $('.collab-module').height();

	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera( 75, newWidth / newHeight, 0.1, 150 );
	camera.position.z = 60;

	var renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( newWidth, newHeight );
	renderer.setClearColor( 0x000000, 0 );
	$('.collab-module').append( renderer.domElement );

	var pointSize = 0.3;

	if (newWidth > 599) {
		pointSize = 0.2;
		camera.position.z = 45;
	}

	var manager = new THREE.LoadingManager();
	// manager.onProgress = function ( item, loaded, total ) {
	// 	// console.log( item, loaded, total );
	// };

	var pointMaterial = new THREE.PointsMaterial( { color: 0x000000, size:pointSize } );


	var objLoader = new THREE.OBJLoader( manager );

	var crossP;
	var cross;


	objLoader.setPath( 'img/' );
	objLoader.load( 'cross.obj', function ( object ) {
		// console.log(object);

		cross = object.children[0];

		var pointGeo = cross.geometry.clone();
		crossP = new THREE.Points( pointGeo, pointMaterial );

		scene.add( crossP );

		// cross.scale.set(0.5, 0.5, 0.5);
		// scene.add( cross );
		// console.log(crossP);

	} );



	var targetRotation=0,
		actualRotation=0,
		actualRotation2=0;

	var render = function () {

		requestAnimationFrame( render );

		// cross1.rotation.x += 0.005;
		// cross.rotation.y += 0.005;

		// cross2.rotation.x += 0.005;
		// cross2.rotation.y += 0.005;

		renderer.render( scene, camera );

		if (mousePcX>0) {
			targetRotation = -0.785 + (1.57*mousePcX);

			actualRotation = actualRotation+((targetRotation - actualRotation)/8);
			// actualRotation2 = actualRotation2+((targetRotation - actualRotation2)/16);


			crossP.rotation.y = actualRotation;
			// cross.rotation.y = actualRotation2;

		}

		if (mousePcY>0) {
			targetRotation = -0.785 + (1.57*mousePcY);
			actualRotation2 = actualRotation2+((targetRotation - actualRotation2)/8);
			crossP.rotation.x = actualRotation2;
		}

		if (gammaRotation) {
			// crossP.rotation.y = 0.785 * gammaRotation;


			// targetRotation =  (1.57*(gammaRotation));
			targetRotation =  (1.57*gammaRotation);


			actualRotation = actualRotation+((targetRotation - actualRotation)/8);
			// actualRotation2 = actualRotation2+((targetRotation - actualRotation2)/16);


			crossP.rotation.y = actualRotation;
			// cross.rotation.y = actualRotation2;
		}

		if (betaRotation) {

			targetRotation =  (1.57*betaRotation);

			actualRotation2 = actualRotation2+((targetRotation - actualRotation2)/8);
			// actualRotation2 = actualRotation2+((targetRotation - actualRotation2)/16);


			crossP.rotation.x = actualRotation2;

		}

	};

	window.addEventListener( 'resize', function () {

		newWidth = $('.collab-module').width();
		newHeight = $('.collab-module').height();

		camera.aspect = newWidth / newHeight;
		camera.updateProjectionMatrix();

		renderer.setSize( newWidth, newHeight );


		//setup headline animation
		TweenMax.killAll();
		distToMoveHeadline = $('.headline__animation h4').width() + 40;
		TweenMax.fromTo('.headline__animation', 6, {x:0},{ease: Power0.easeNone, x:-distToMoveHeadline, repeat: -1});

	}, false );

	render();


};