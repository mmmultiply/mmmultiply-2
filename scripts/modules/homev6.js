app.homeV6 = function() { 

	if ( $('body').is("#index-content") ) {

		$('.header__links').fadeIn('fast');

		// console.log('run home');

		var messageOffset;
		var viewportWidth;
		var maxProjWidth,
			maxProjHeight;

		var getClosest;


		var docPos = $( window ).scrollTop();
		docHeight = $( "body" ).height() - $( window ).height();

		var projectsContent = $(".projects").html();
		var contentLength = $(".projects").children().length;

		windowHeight = $(window).height();

		var projSingle = e.homeV6.find(".projects__single");

		var projImgs = projSingle.find('img');
	    var imgLoop = projImgs.length;

	    var changeTo;


		// event listeners

		// on scroll - constantly fires on scroll
		$( window ).scroll( function() {
			docPos = $( window ).scrollTop();
			moveProjects();
			xParallax();
		});


		// on resize
		$( window ).resize( function() {
			
			windowHeight = $(window).height();
			setupOffset();
			docPos = $( window ).scrollTop();
			docHeight = ($( document ).height()) - ($( window ).height());
			setupProjectSpaceing();
			xParallax();

		});

		$('.projects__single').click(function() {

			$(".hero-content").css('opacity','0');

			var projectHeight = e.homeV6.find('.projects__single').height();


			scrollToPosition = $( this ).offset().top - ((windowHeight - projectHeight)/2);

			// console.log(scrollToPosition);
			// console.log($( this ).offset().top);


			$('html, body').animate({
		        scrollTop: scrollToPosition
		    }, 200);
		});


		// init
		setupOffset();
		hoverFunctions();
		setupProjectSpaceing();
		moveProjects();
		xParallax();


		// console.log($(".projects"));

		function setupProjectSpaceing() {

			e.homeV6.find('.projects').css('padding-top', '80px' ).css('padding-bottom', '230px' );
			// console.log(windowWidth);
			if (windowWidth > 767) {
				var projectHeight = e.homeV6.find('.projects__single').height();
				var newProjectMargin = (windowHeight - projectHeight)/2 + "px";
				e.homeV6.find('.projects').css('padding-top', newProjectMargin ).css('padding-bottom', newProjectMargin );
			}
			
		}


		function setupOffset() {
			viewportWidth = $(window).width(); 

			messageOffset = e.homeV6.find(".top-cat").height();
		}

		function hoverFunctions() {
			e.homeV6.find(".projects__single").hover(	
				function(){
					$(".projects__single").removeClass('active-middle');
					projectCatSelection( $(this ) );
			    	$(this ).addClass('active');
				},
				function() { 
					$( this ).removeClass('active'); 
					moveProjects();
				}
			);
		};


		function getMaxWidth() {
			maxProjWidth = (e.homeV6.find('.projects').width());
			maxProjHeight = (maxProjWidth/16) * 9;
			// console.log(maxProjWidth);
			// console.log(maxProjHeight);
		}

		function closest(array, number) {
	        var num = 0;
	        for (var i = array.length - 1; i >= 0; i--) {
	            if(Math.abs(number - array[i].position) < Math.abs(number - array[num].position)){
	                num = i;
	            }
	        }
	        return array[num].element;
	    }

	    function moveProjects() {

			getMaxWidth();

		    // console.log('moveProjects');
			var scrollTop = $(document).scrollTop() + ($(window).height() / 2);
			var positions = [];

			e.homeV6.find(".projects__single").each(function(){
			//console.log($(this).position().top);
				// $(this).removeClass("active-middle");
				positions.push({
					position:$(this).position().top + maxProjHeight/2, 
					element: $(this)
				});
			});

			var oldGetClosest = getClosest;

				getClosest = closest(positions,scrollTop);


				if (!getClosest.is(oldGetClosest)) {

					e.homeV6.find(".projects__single").removeClass("active-middle");

					projectCatSelection( getClosest );
					getClosest.addClass("active-middle");
					$( '.projects__single' ).removeClass('active'); 
				}
			// console.log(getClosest);



	    }

	    function projectCatSelection( elem ) {
	    	var changeToPrevious = changeTo;

	    	var sameCat = false;

	    	changeTo = String(elem.attr('data-cat'));

	    	// if () {
	    	// 	sameCat = true;
	    	// }

	    	var elemCounter = 0;
	    	var elemLength = e.homeV6.find('.cat').length;
	    	// e.homeV6.find('.cat').stop( true, true );


	    	if (changeToPrevious !== changeTo) {

	    		// console.log('changeToPrevious = '+changeToPrevious);
	    		// console.log('changeTo = '+changeTo);

	    		// console.log('select a different catergory');

	    		e.homeV6.find('.cat').fadeOut(300, function() {

	    			elemCounter ++;

	    			if (elemCounter == elemLength-1) {


	    				if (changeTo == "brand") {

	    					e.homeV6.find('.cat--brand').delay(300).fadeIn(300);

	    				} else if (changeTo == "campaign") {

	    					e.homeV6.find('.cat--campaign').delay(300).fadeIn(300);

	    				} else if (changeTo == "motion") {
	    					
	    					e.homeV6.find('.cat--motion').delay(300).fadeIn(300);

	    				}
	    				 else if (changeTo == "digital") {
	    					
	    					e.homeV6.find('.cat--digital').delay(300).fadeIn(300);

	    				} else {

	    					e.homeV6.find('.cat--brand').delay(300).fadeIn(300);

	    				}
	    			}
	    			
	    		});

	    		// e.homeV6.find('.cat').css('filter','blur(3px)').fadeOut(300, function() {

	    		// 	elemCounter ++;

	    		// 	if (elemCounter == elemLength-1) {


	    		// 		if (changeTo == "brand") {

	    		// 			e.homeV6.find('.cat--brand').delay(300).css('filter','blur(0px)').fadeIn(300);

	    		// 		} else if (changeTo == "campaign") {

	    		// 			e.homeV6.find('.cat--campaign').delay(300).css('filter','blur(0px)').fadeIn(300);

	    		// 		} else if (changeTo == "motion") {
	    					
	    		// 			e.homeV6.find('.cat--motion').delay(300).css('filter','blur(0px)').fadeIn(300);

	    		// 		}
	    		// 		 else if (changeTo == "digital") {
	    					
	    		// 			e.homeV6.find('.cat--digital').delay(300).css('filter','blur(0px)').fadeIn(300);

	    		// 		} else {

	    		// 			e.homeV6.find('.cat--brand').delay(300).css('filter','blur(0px)').fadeIn(300);

	    		// 		}
	    		// 	}
	    			
	    		// });
	    	}

	    }

	    function xParallax() {
	    	// console.log(docHeight);
	    	var xPosTop = ((docPos/docHeight)*300)+70;
	    	xPosTop = xPosTop +"px";
	    	e.homeV6.find('.bg-cross').css('top', xPosTop);

	    }

    }

    var iframe = document.querySelector('iframe');
    var player = new Vimeo.Player(iframe);

    e.homeV6.find('.show-reel').on('click touch', function () {
    	e.homeV6.find('.show-reel__container').addClass('active');
    	player.play();
    })

    e.homeV6.find('.show-reel__close, .show-reel__container').on('click touch', function () {
    	e.homeV6.find('.show-reel__container').removeClass('active');
    	$(".hero-content").css('opacity','1');
    	player.pause()
    })







	var timeout;

	// var tilt = $('.bg-cross').tilt(); 

	// $(window).resize(changePersective);

	// changePersective();

	// function changePersective(){
	//   TweenMax.set('.bg-cross', {perspective: $('.bg-cross').width()});
	// }


	// $('.bg-cross').mousemove(function(e){
	//   if(timeout) clearTimeout(timeout);
	//   setTimeout(callParallax.bind(null, e), 10);
	  
	// });

	// function callParallax(e) {
	//   parallaxIt(e, '.bg-cross', 50);
	//   // parallaxIt(e, 'img', 10);
	// }

	// function parallaxIt(e, target, movement){
	//   var $this = $('.bg-cross');
	//   var relX = e.pageX - $this.offset().left;
	//   var relY = e.pageY - $this.offset().top;
	  
	//   TweenMax.to(target, 1, {
	//     rotationY: (relX - $this.width()/2) / $this.width() * movement,
	//     rotationX: -(relY - $this.height()/2) / $this.height() * movement,
	//     ease: Power2.easeOut
	//   })
	// }

	// $(".bg-cross").hover( 

	// 	function() {
	// 		console.log('hover on');
	// 	},

	// 	function() { 
	// 		TweenMax.to('.bg-cross', 1, {
	// 		  rotationY: 0,
	// 		  rotationX: 0,
	// 		  ease: Power2.easeOut
	// 		})
	// 	}
	// )
};