app.work = function() {

	console.log('run work script');

	var pathname = window.location.href;
	var filter = "initial";

	if(pathname.indexOf("#") > -1) {
		filter = pathname.split('#').pop();
   	}

	//init
	filterProjects();
	//event listeners

	//run filter projects on filter link click
	$('.project-filter a').click(function() {


		// pathname = window.location.href;
		pathname = $(this).attr('href');

		filter = pathname.split('#').pop();
		console.log('find '+ filter);

		filterProjects();
		// $('.project-filter a').removeClass('active');
		// $(this).addClass('active');

	});

	// Cycle plugin
	var cCycle = "pause",
		cSpeed = 1400,
		delay = 0;

    if (is_touch_device()) {
    	var cCycle = "resume";
		cSpeed = 1500;
		delay = 400;

    } 

    $.each($('.slides'), function(i, el){

        $(el).cycle({
		    fx:     'none',
		    speed:   cSpeed,
		    timeout: 1,
		    delay: 	 i * delay,
		    log: 	 false
		}).cycle(cCycle);

    });


    if (is_touch_device()) {
    	$('.slides').addClass('active').cycle('resume');
    } 

    // Pause & play on hover
    $('.projects__single').hover(function(){
        $(this).find('.slides').addClass('active').cycle('resume');
    }, function(){
        $(this).find('.slides').removeClass('active').cycle('destroy');
        $(this).find('.slides').cycle({
		    fx:     'none',
		    speed:   cSpeed,
		    timeout: 1,
		    delay: 	 delay,
		    log: 	 false
		}).cycle(cCycle);
    });




	function filterProjects() {


		console.log('filter projects');



		if (filter == "brands" || filter == "vfx" || filter == "digital" || filter == "advertising" || filter == 'design' ) {


			// e.work.find('.projects__single').css("opacity", "0.15");
				
			// $('[data-filter='+filter+']').css("opacity", "1");				


			//old above
			//new below

			e.work.find('.projects__single')
				.animate({opacity:"0"}, 0)
				// .css('filter','blur(5px)')
				.delay(300)
				.hide(0, function() {
					$("[data-filter*='"+filter+"']")
						// .filter("[data-filter*='"+filter+"']")
					.show(10)
					.animate({opacity:"1"}, 0)
					// .css('filter','blur(0px)')

				});
			$('.project-filter a').removeClass('active');
			$('a[href*="'+filter+'"]').addClass('active');


			// console.log('find '+ filter);
		}	else if (filter == "everything") {
			e.work.find('.projects__single')
				.css("opacity","0")
				// .css('filter','blur(5px)')
				.delay(300)
				.show(10)
				.animate({opacity:"1"}, 1)
				// .css('filter','blur(0px)');
			$('.project-filter a').removeClass('active');
			$('a[href*="'+filter+'"]').addClass('active');
		}	else if (filter == "initial") {
			$('a[href*="everything"]').addClass('active');
		}

		// console.log('filter projects funtions');
	}

	function filterAnimation(filter) {
		$('[data-filter='+filter+']')
			.show(10)
			.animate({opacity:"1"}, 400);
	}



}