app.contentPage = function() {
	// console.log('run content page');

	lightbox.option({
      'resizeDuration': 0,
      'wrapAround': true,
      'disableScrolling': true,
      'fadeDuration': 50,
      'positionFromTop': 0
    })

	//event listeners
	$( window ).resize( function() {

		setRevealerHeights();

	});

	$('#intro-canvas').css('opacity', '0');

	// var filterOptions = e.content.find('h2');

	// $.each(filterOptions, function(i, elem) {

	// 	var thelink = '<h3><a class="" href="#'+($(elem).text()).replace(/\s/g, '')+'">'+$(elem).html()+'</a></h3>';

	// 	e.content.find('.content-filter').append(thelink);

	// 	// if ( i == filterOptions.length-1 ) {

	// 	// 	$.each($('a[href^="#"]'), function(i, elem) {
	// 	// 		elem.addEventListener('click', function (e) {
	// 	// 	        e.preventDefault();

	// 	// 	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	// 	// 	            behavior: 'smooth'
	// 	// 	        });
	// 	// 	    });
	// 	// 	});

	// 	// 	// console.log('final filter and apply smooth scroll');
	// 	// 	// document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	// 	// 	//     anchor.addEventListener('click', function (e) {
	// 	// 	//         e.preventDefault();

	// 	// 	//         document.querySelector(this.getAttribute('href')).scrollIntoView({
	// 	// 	//             behavior: 'smooth'
	// 	// 	//         });
	// 	// 	//     });
	// 	// 	// });
	// 	// }
	// });

	var videoContainer = document.getElementsByClassName("content__layer-reveal");
	var videoClipper = $("#video_two");

	if (videoContainer != "") {
		var clippedVideo = videoClipper.find("video");

		for (var i = 0; i < videoContainer.length; i++) {
			videoContainer[i].addEventListener( "mousemove", trackLocation, false); 
			videoContainer[i].addEventListener( "mousemove", trackLocation, false); 
			videoContainer[i].addEventListener("touchstart",trackLocation,false);
			videoContainer[i].addEventListener("touchmove",trackLocation,false);
		}
	}

	function trackLocation(e) {
		for (var i = 0; i < videoContainer.length; i++) {
			var rect = videoContainer[i].getBoundingClientRect(),
			position = ((e.pageX - rect.left) / videoContainer[i].offsetWidth)*100;
		}
		if (position <= 100) { 
			var videoClipper = $("#video_two");
			videoClipper.css( "width", position+"%");
			clippedVideo.css("width", ((100/position)*100)+"%");
			 // console.log(position);
			$('.content__layer-reveal__handle').css( 'left', position+"%" );
			// clippedVideo.css().zIndex = 3;
		}
	}
	

	setRevealerHeights();


	function setRevealerHeights() {
		var layerRevealers = e.content.find('.content__layer-reveal');

		layerRevealers.css( "height", layerRevealers.first().width()*0.5625 + "px" );
		e.content.find('.content__layer-reveal__top').css("width", layerRevealers.first().width() );
	}

	// console.log($('.content__item--title'));
	inView('.content__item--title')
    .on('enter', function(el){ $(el).addClass('title-load') })
    .on('exit', function(el) { $(el).removeClass('title-load') });
}
