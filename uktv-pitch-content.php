<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">UKTV Pitch Presentation</h1>
			<h4 class='content-filter__sub-title'>A breakdown of the UKTV channel family</h4>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-UKTV-Pitch-Pressentation_1.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply-UKTV-Pitch-Pressentation_1.jpg" data-lightbox="uktv">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-UKTV-Pitch-Pressentation_2.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply-UKTV-Pitch-Pressentation_2.jpg" data-lightbox="uktv">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>We were asked to create a breakdown of the UKTV channel family for marketing and communication potential.</p>
				</div>
				
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-UKTV-Pitch-Pressentation_3.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply-UKTV-Pitch-Pressentation_3.jpg" data-lightbox="uktv">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-UKTV-Pitch-Pressentation_4.jpg" data-lightbox="uktv">
						<img src="img/MMMultiply-UKTV-Pitch-Pressentation_4.jpg" data-lightbox="uktv">
					</a>
				</div>

				

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="fox-agentcarter.php" data-pageid="content-page" class="ajax-link">
				<span>Fox Agent Carter</span>
				<img src="img/MMMultiply_FOX-AgentCarter-4_thumb.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3  projects__single projects__single--thumb  init-load inview">
			<a href="national-geographic-magazine.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Magazine</span>
				<img src="img/MMMultiply-NatGeo-Mag-10.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="returntothenile-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Return to the Nile Campaign</span>
				<img src="img/MMMultiply-ReturnToTheNileCampaign-01.jpg">
			</a>
		</div>
	</div>
</section>