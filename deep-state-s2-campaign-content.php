<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Deep State S2 Campaign</h1>
			<p>
				Following on from the success of season one, we worked closely with the team at FOX to deliver the much anticipated Deep State series 2 campaign. In keeping with the general theme of visual trickery, the key art was adapted to the location and setting of the new series, using the form of sand to represent the elusive nature of the Deep State. 
			</p>
			<p>
				We developed the key art from initial concepts through to delivery, as well as creating the toolkit for FOX that allowed them to adapt the artwork for different territories. 
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">
				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/330747703?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 content__item init-load inview">
					<h3>Truth is a matter of perspective.</h3>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateS2-01.jpg" data-lightbox="DeepStateS2">
						<img src="img/MMMultiply-DeepStateS2-01.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateS2-02.gif" data-lightbox="DeepStateS2">
						<img src="img/MMMultiply-DeepStateS2-02.gif">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateS2-03.gif" data-lightbox="DeepStateS2">
						<img src="img/MMMultiply-DeepStateS2-03.gif">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateS2-04.jpg" data-lightbox="DeepStateS2">
						<img src="img/MMMultiply-DeepStateS2-04.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateS2-05.jpg" data-lightbox="DeepStateS2">
						<img src="img/MMMultiply-DeepStateS2-05.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateS2-06.jpg" data-lightbox="DeepStateS2">
						<img src="img/MMMultiply-DeepStateS2-06.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateS2-07.jpg" data-lightbox="DeepStateS2">
						<img src="img/MMMultiply-DeepStateS2-07.jpg">
					</a>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State S1 Campaign</span>
				<img src="img/MMMultiply-DeepStateATL-01.jpg">
				
			</a>
		</div>

		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-titles.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State Titles</span>
				<img src="img/MMMultiply-DeepStateTitles-01.jpg">
				
			</a>
		</div>
				<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="returntothenile-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Return to the Nile Campaign</span>
				<img src="img/MMMultiply-ReturnToTheNileCampaign-01.jpg">
			</a>
		</div>
	</div>
</section>