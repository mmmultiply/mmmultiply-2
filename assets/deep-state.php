<?php 
  include 'template-parts/header-html.php';
?>

<body id="content-page">

<?php 
  include 'template-parts/header.php';
  include 'deep-state-content.php';
  include 'template-parts/footer.php';
?>

</body>

<?php 
  include 'template-parts/footer-html.php';
?>