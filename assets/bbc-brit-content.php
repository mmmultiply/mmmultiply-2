<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load">
			<h1 class="h5">BBC Brit Rebrand</h1>
			<h4 class="content-filter__sub-title">Brand refresh for BBC Brit in South Africa</h4>
				
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/257115907?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load inview">
					<h3 class="">
						Best of British TV<br>with a local flavour.
					</h3>
				</div>
 
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-BBCBrit-03.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-03.jpg">
					</a>
				</div>

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/257115908?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>


				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/LightFantastic_Logo_4.jpg" data-lightbox="bbc brit">
						<img src="img/LightFantastic_Logo_4.jpg">
					</a>
				</div>

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/257115929?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>


				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/Toys_04.jpg" data-lightbox="bbc brit">
						<img src="img/Toys_04.jpg">
					</a>
				</div>

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/257115952?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>


				<div class="col-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-07.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-07.jpg">
					</a>
				</div>
				<div class="col-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-10.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-10.jpg">
					</a>
				</div>



				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-09.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-09.jpg">
					</a>
				</div>

				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-11.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-11.jpg">
					</a>
				</div>
				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-bumpers.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-bumpers.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-guide-01.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-guide-01.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-guide-02.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-guide-02.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-guide-03.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-guide-03.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-BBCBrit-guide-04.jpg" data-lightbox="bbc brit">
						<img src="img/MMMultiply-BBCBrit-guide-04.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
				
					<p>BBC BRIT was expanding from being a male skewing factual entertainment channel to include a more balanced range of entertainment shows such as Graham Norton along with local commissions like Strictly Come Dancing SA.</p>

					<p>Our brief was to refresh the brand to clearly reflect the channels new direction while quashing preconceptions that the BBC is a stuffy British brand.</p>

					<p>Working closely with the marketing and creative departments we created a striking new identity for BRIT, with a vibrant new colour palette, bold typography and OSP along with a stunning new set of idents which beautifully reflected the channels core message, zest for life. The premium look was better aligned with the new content set while giving just enough of a local flavour to stay connected with the South African audience.</p>
				</div>

						

			</div>
			
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-3 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single  projects__single--thumb init-load inview">
			<a href="bbc-lifestyle.php" data-pageid="content-page" class="ajax-link">
				<span>BBC Lifestyle Rebrand</span>
				<img src="img/MMMultiply-BBCLifestyle-ID03-02.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single  projects__single--thumb init-load inview">
			<a href="challenge.php" data-pageid="content-page" class="ajax-link">
				<span>Challenge Rebrand</span>
				<img src="img/MMMultiply-Challenge-Modern_1.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="pick.php" data-pageid="content-page" class="ajax-link">
				<span>Pick Rebrand</span>
				<img src="img/MMMultiply-Pick-01-ID-Popcorn.jpg">
			</a>
		</div>
	</div>

</section>