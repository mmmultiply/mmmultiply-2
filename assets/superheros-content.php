<section class="container ajax">
	<div class="row flex-md-row-reverse">

		<div class="content-filter col-12 col-md-4 init-load">
			<h1 class="h2">Deep State Title Sequence</h2>
		</div>

		
		<div class="content col-12 col-md-8 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/263478354" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-02.jpg">
				</div>
				<div class="col-12 col-sm-6 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-07.jpg">
				</div>
				<div class="col-12 col-sm-6 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-09.jpg">
				</div>
				<div class="col-12 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-06.jpg">
				</div>
				<div class="col-12 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-08.jpg">
				</div>
				<div class="col-12 col-sm-6 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-11.jpg">
				</div>
				<div class="col-12 col-sm-6 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-12.jpg">
				</div>
				<div class="col-12 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-03.jpg">
				</div>
				<div class="col-12 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-10.jpg">
				</div>
				<div class="col-12 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-13.jpg">
				</div>
				<div class="col-12 content__item init-load">
					<img src="img/MMMultiply-DeepStateTitles-01.jpg">
				</div>

				<div class="col-12 content__item init-load">
					<h2 id="breakdown">Breakdown</h2>	
					<div class="content__layer-reveal">
					  <img src="img/MMMultiply-DeepStateTitles-04.jpg">
					  <div id="video_two">
					  	<img class="content__layer-reveal__top" src="img/MMMultiply-DeepStateTitles-05.jpg">
					  </div>  
					</div>		
				</div>

				<div class="col-12 content__item init-load">
					<h2 id="moreinformation">More information</h2>			
					<p>
						Working closely with FOX UK we created the title treatment and designed and animated the title sequence for the brilliant new espionage thriller Deep Sate.
					</p>
					<p>
						Deceit, power and conspiracy drive the new series so we created an introduction that challenges perceptions and obscures reality, slowly revealing the dark secrets that are hidden deep within.
					</p>		
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 related-work__title">
			<h3>
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 projects__single init-load">
			<a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply-BBCBrit-01.jpg">
				<span>BBC Brit</span>
			</a>
		</div>
		<div class="col-12 col-sm-4 projects__single init-load">
			<a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply-BBCBrit-01.jpg">
				<span>BBC Brit</span>
			</a>
		</div>
		<div class="col-12 col-sm-4 projects__single init-load">
			<a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply-BBCBrit-01.jpg">
				<span>BBC Brit</span>
			</a>
		</div>
	</div>
</section>