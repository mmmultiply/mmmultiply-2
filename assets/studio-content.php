<section class="ajax">
	<div id="photosphere" class="photosphere studio-map init-load">
			
	</div>

	<div id="concept" class="studio-info">
		<div class="studio-info__wrapper">
			<div class="studio-info__header">
				<h2>
					The Process
				</h2>

				<div class="close">
				</div>
			</div>
			<div class="studio-info__content">
				<div>
					<p >
						Our design process starts with discovery. By basing our work on a solid foundation of research we can create truly engaging experiences that connect with the audience on a personal level. 
					</p>
					<p>
						We developed this “Smoke and Mirrors” concept for the Deep State title sequence, based on the emotive response to the Deep State of how a dark and mysterious entity holds control of the world by obscuring reality. 
					</p>
					<br>
					<h3>
						Deep State Concept
					</h3>
					 <a href="deep-state-titles.php" data-pageid="content-page" class="ajax-link"><img src="img/deepstate_concept.jpg"></a>
					<br>
					<p>
						Go to <a href="deep-state-titles.php" data-pageid="content-page" class="ajax-link link-navigation">Deep State Title Sequence</a>
					</p>
				</div>
				

			</div>
		</div>
		
	</div>

	<div id="planning" class="studio-info">
		<div class="studio-info__wrapper">
			<div class="studio-info__header">
				<h2>
					The Strategy
				</h2>

				<div class="close">
				</div>
			</div>
			<div class="studio-info__content">


				<div>
					<p>
						To provide the best service to our clients we operate as a full service agency, with over 30 years of combined experience working on projects of all sizes, from the bespoke to the global campaigns.
					</p>
					<br>
					<h3>
						Fox Plus Campaign
					</h3>
					 <a href="fox-plus.php" data-pageid="content-page" class="ajax-link"><img src="img/MMMultiply-Fox-Plus-07.jpg"></a>
					<br>
					<p >
						Go to <a href="fox-plus.php" data-pageid="content-page" class="ajax-link link-navigation">Fox Plus</a>
					</p>
				</div>
				
			</div>
		</div>
		
	</div>

	<div id="design" class="studio-info">
		<div class="studio-info__wrapper">
			<div class="studio-info__header">
				<h2>
					The Art
				</h2>

				<div class="close">
				</div>
			</div>
			<div class="studio-info__content">
				<div>
					<p >
						Our team of designers are specialists in an array of disciplines, whether it is a solution based in 3d, print or digital we are able to create an informed and fitting response that drives brands to engage with their audience.
					</p>
					<br>
					<h3>
						BBC Brit Brand
					</h3>
					 <a href="fox-plus.php" data-pageid="content-page" class="ajax-link"><img src="img/MMMultiply-BBCBrit-logo.png"></a>
					<br>
					<p >
						Check out the <a href="bbc-brit.php" data-pageid="content-page" class="ajax-link link-navigation">BBC Brit rebrand</a><br><br>Or see what were playing with <a href="https://www.instagram.com/mmmultiply" data-pageid="content-page" class="link-navigation">on instagram</a>
					</p>
				</div>
			</div>
		</div>
		
	</div>

	<div id="vfx" class="studio-info">
		<div class="studio-info__wrapper">
			<div class="studio-info__header">
				<h2>
					The Magic
				</h2>

				<div class="close">
				</div>
			</div>
			<div class="studio-info__content">
				<div>
					<p >
						3D design is a powerful part of our capabilities and allows us to create anything we can imagine. When you combine specialist 3D knowledge with our carefully crafted ideas and visual identities there is no limit to what we can achieve. 
					</p>
					<br>
					<h3>
						Heroes and Villains
					</h3>
					 	<div style="padding:56.25% 0 0 0;position:relative;">
					 		<iframe src="https://player.vimeo.com/video/257115699" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					 	</div>
					<br>
					<br>
					<p >
						Go to <a href="heroes-and-villains.php" data-pageid="content-page" class="ajax-link link-navigation">Heroes &amp; Villains</a>
					</p>
				</div>
			</div>
		</div>
		
	</div>

	<div id="bundesliga" class="studio-info">
		<div class="studio-info__wrapper">
			<div class="studio-info__header">
				<h2>
					Bundesliga
				</h2>

				<div class="close">
				</div>
			</div>
			<div class="studio-info__content">
				<div>
					<p >
						We modeled this ball for the promo for Fox's coverages of the Bundesliga. 
					</p>
					<br>
					 	<div style="padding:56.25% 0 0 0;position:relative;">
					 		<iframe src="https://player.vimeo.com/video/169085842?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					 	</div>
					<br>
					<br>
					<p >
						Go to <a href="bundesliga.php" data-pageid="content-page" class="ajax-link link-navigation">Bundesliga</a>
					</p>
				</div>
			</div>
		</div>
		
	</div>

	<div id="ea" class="studio-info">
		<div class="studio-info__wrapper">
			<div class="studio-info__header">
				<h2>
					EA FIFA 2019
				</h2>

				<div class="close">
				</div>
			</div>
			<div class="studio-info__content">
				<div>
					<p >
						Check out this campaign work we did for EA's latest instalment of FIFA.
					</p>
					<br>
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/295805182" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

					<br>
					<br>
					<p >
						Go to <a href="ea-fifa19.php" data-pageid="content-page" class="ajax-link link-navigation">FIFA 19</a>
					</p>
				</div>
			</div>
		</div>
		
	</div>

	<div id="bbc" class="studio-info">
		<div class="studio-info__wrapper">
			<div class="studio-info__header">
				<h2>
					BBC Brit
				</h2>

				<div class="close">
				</div>
			</div>
			<div class="studio-info__content">
				<div>
					<p >
						Working with the in house design and marketing department at BBC Worldwide we helped refresh the BBC BRIT brand. This ident was part of a set of five.
					</p>
					<br>
					 	
						<div style="padding:56.25% 0 0 0;position:relative;">
							<iframe src="https://player.vimeo.com/video/257115952" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
						</div>

					<br>
					<br>
					<p >
						Go to <a href="bbc-brit.php" data-pageid="content-page" class="ajax-link link-navigation">BBC Brit</a>
					</p>
				</div>
			</div>
		</div>
		
	</div>

	<!-- <div class="studio-info-nav sofas">

		<div class="studio-info-nav__label studio-info-nav__label--area label-sofas" label="sofas">
			<div class="studio-info-nav__number">
				1.0
			</div>
			<div class="studio-info-nav__title">
				Sofas
			</div>
		</div>

		<div class="studio-info-nav__label label-concept" label="concept">
			<div class="studio-info-nav__number">
				1.1
			</div>
			<div class="studio-info-nav__title">
				Concept
			</div>
		</div>

		<div class="studio-info-nav__label label-planning" label="planning">
			<div class="studio-info-nav__number">
				1.2
			</div>
			<div class="studio-info-nav__title">
				Planning
			</div>
		</div>

		<div class="studio-info-nav__label studio-info-nav__label--area label-studio" label="studio">
			<div class="studio-info-nav__number">
				2.0
			</div>
			<div class="studio-info-nav__title">
				Studio
			</div>
		</div>

		<div class="studio-info-nav__label label-design" label="design">
			<div class="studio-info-nav__number">
				2.1
			</div>
			<div class="studio-info-nav__title">
				Design
			</div>
		</div>

		<div class="studio-info-nav__label label-vfx" label="vfx">
			<div class="studio-info-nav__number">
				2.2
			</div>
			<div class="studio-info-nav__title">
				3D &amp; VFX
			</div>
		</div>
	</div> -->
	
</section>


