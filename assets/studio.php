<?php 
  include 'template-parts/header-html.php';
?>

<body id="studio-page">

<?php 
  include 'template-parts/header.php';
  include 'studio-content.php';
  include 'template-parts/footer.php';
?>

</body>

<?php 
  include 'template-parts/footer-html.php';
?>