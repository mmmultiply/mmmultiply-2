<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Sky Sports 2019 Preview</h1>
			<p>
				Description Here.
			</p>
			<p>
				Description Here.
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/309903619?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>


			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-titles.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State Titles</span>
				<img src="img/MMMultiply-DeepStateTitles-01.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="work.php#advertising">
				<span>More Campaigns</span>
				<img src="img/MMMultiply-cross.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="suicide-squad.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema deep state atl</span>
				<img src="img/MMMultiply-SS-Cover.jpg">
				
			</a>
		</div>
	</div>
</section>