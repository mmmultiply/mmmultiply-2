<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load">
			
                <h3><a href="#theinformationthatwecollect">
                    The information that we collect
                </a></h3>

                <h3><a href="#useofcookies">Use Of Cookies</a></h3>
                <h3><a href="#useofyourinformation">Use Of Your Information</a></h3>
                <h3><a href="#accesstoinformation">Access To Information</a></h3>
                <h3><a href="#contactingus">Contacting Us</a></h3>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row">

                <div class="col-12 content__item content__item--title init-load inview">
                    <h3 class="">
                        A very private statement
                    </h3>
                </div>

				<div class="col-12 content__item init-load">
					<p>At MMMultiply we take your privacy and the trust you put in us seriously. We want to make sure that when you visit our site or communicate with us in any way, you know that we keep all your personal information a secret. Trust us, our lips are sealed.</p>

					<p>This Privacy Policy explains what happens to any personal data that you provide to us, or that we might collect from you</p>

					<p>We will update this Policy every now and again when it’s required. But don’t worry, this is not a regular thing and we will let you know if we make any major changes so you can come back and enjoy reading it again.</p>
				</div>

				<div class="col-12 content__item init-load">
					<h4 id="theinformationthatwecollect">The information that we collect</h4>
					<p>So that we’re up to date with the latest privacy guidelines, our website is compliant with the <a href="https://www.eugdpr.org/" target="_blank">General Data Protection Regulations</a> (GDPR). We use <a href="https://analytics.google.com/analytics/web/provision/?authuser=0#provision/SignUp/" target="_blank">Google Analytics</a> on the website (please <a href="https://www.google.com/analytics/terms/us.html" traget="_blank">click here</a> to read their Privacy Policy) to collect and process the following information about you:</p>
					<ul>
                        <li>
                            Details of your visit to this website &amp; the resources that you accessed; including, but not limited to: traffic data, location data, hardware and software being used and other communication data.
                        </li>
                    </ul>
                    <p>None of this information allows us to identify you specifically as an individual, so we don’t know your name, what you’re wearing, or what your favourite colour is for example, like some other well known sites.</p>
                    <h4 id="useofcookies">
                        Use of Cookies
                    </h4>
                    <p>
                        As mentioned above, we gather information about your computer for our website and to provide statistical information regarding the use of our website so that we can better tailor the experience to our visitors in general.
                    </p>
                    <p>The information does not identify you personally. It is statistical data about visitors and their use of our site. The data won’t let us know if your’e a cat person or a dog person, but it will let us know how to improve our services so they are more relevant to the people who come and have a look around. We’re a friendly bunch so we aways love new visitors. This is something everyone is doing, just a little way of us making your life better without you even noticing.</p>
                    <p>If you chose you don’t want to accept these cookies, then don’t worry, every computer has the ability to decline cookies. This can be done by activating a setting on your browser which enables you to decline the cookies. If you choose to decline cookies, you might be unable to access some parts of our website and you wouldn’t want to miss out on that. But it’s totally up to you.</p>
                    <h4 id="useofyourinformation">Use of Your Information</h4>
                    <p>Any information we collect and store directly relating to you is usually collected when you contact us via email, phone, or other form of physical or digital communication - yes, maybe you’ve sent an actual letter, or Skyped us. Alternatively, it may be sourced from freely accessible information about you from the internet.</p>
                    <p>This data is used to enable us to provide our services to you or to contact you when there is relevant work. We may use this information for the following purposes:</p>
                    <ul>
                        <li>To notify you about any changes to our website, such as improvements or service/product changes, that may affect our service. Which is good to know.</li>
                        <li>To contact you should be feel work we have available is relevant.</li>
                        <li>To acknowledge you on any featured projects on any of our sites or social channels if relevant to do so. </li>
                    </ul>
                    <p>Data that is provided to us is stored on secure servers, safely guarded by technology designed to do so, so you don’t have to worry. However, we cannot guarantee the security of data sent to us electronically. Even though we are really trustworthy, the internet is less so. So, transmission of such data is therefore sent entirely at your own risk. </p>
                    <h4 id="accesstoinformation">Access To Information</h4>
                    <p>The <a href="http://www.legislation.gov.uk/ukpga/1998/29/contents" target="_blank">Data Protection Act 1998</a> ensures strict adherence to the 8 principles that underpin it. Data must:</p>
                    <ul>
                        <li>Be processed fairly and lawfully</li>
                        <li>Be obtained only for specified, lawful purposes</li>
                        <li>Be adequate, relevant and not excessive</li>
                        <li>Be accurate and updated regularly</li>
                        <li>Not be held for any longer than required</li>
                        <li>Be processed according to the rights of the data subjects</li>
                        <li>Be protected accordingly</li>
                        <li>
                            Not be transferred outside the European Economic Area (EEA), unless the destination complies with adequate protection criteria and the <a href="https://www.eugdpr.org/" target="_blank">General Data Protection Regulations</a> (GDPR) gives you:
                              <ul>
                                <li>the right of access to the information that we hold about you</li>
                                <li>the right of rectification/correction of the information that we hold about you</li>
                                <li>the right of erasure/to be forgotten</li>
                                <li>the right of restriction on processing</li>
                                <li>the right to object to processing</li>
                                <li>the rights of portability/wanting your data</li>
                            </ul>
                          </li>
                    </ul>
                    <p>If this is applicable, bear in mind, that the information we have about you can simply be your name and your email address, (exciting stuff we know.) If you decide you want to find out a little more about the details we have about you then you can send us an email at the details below.</p>
                    <h4 id="contactingus">Contacting Us</h4>
                    <p>If you’re still reading (thanks for sticking with us by the way) and you have some questions regarding this policy then you can email us at <a href="mailto:studio@mmmultiply.com">studio@mmmultiply.com</a> and we’ll get back to you asap.
                    </p>
                    <p>Now that’s all out the way, we’ll get back to what we’re good at, creating beautifully crafted creative for our amazing clients.
                    </p>
                    <p>MMMultiply Team</p>
                    <p>This document was last updated on May 23, 2018.</p>
				</div>				

			</div>
			
		</div>
	</div>
	<div class="row">
        <div class="col-12 col-lg-9 offset-lg-3 related-work__title">
            <h4 class="h5">
                Related Work
            </h4>
        </div>
        
    </div>
    <div class="row related-work projects projects--3">
        <div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
            <a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
                <span>BBC Brit</span>
                <img src="img/MMMultiply-BBCBrit-01.jpg">
                
            </a>
        </div>
        <div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
            <a href="work.php#brands">
                <span>More Brands</span>
                <img src="img/MMMultiply-cross.jpg">
                
            </a>
        </div>
        <div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
            <a href="fox-plus.php" data-pageid="content-page" class="ajax-link">
                <span>Fox Plus Brand</span>
                <img src="img/MMMultiply-Fox-Plus-logo.jpg">
                
            </a>
        </div>
    </div>
</section>