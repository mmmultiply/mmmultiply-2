<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">EA FIFA 19</h1>
			<h4 class="content-filter__sub-title">A promotional video for EA designed drive engagement and boost the excitment surrounding the launch of FIFA 19.</h4>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/295805182" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load inview">					
					<a href="img/MMMultiply_EA_FIFA19_01.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply_EA_FIFA19_01.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<h3 class="">
						"You might know this one..."
					</h3>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">

					<a href="img/MMMultiply_EA_FIFA19_08.jpg" data-lightbox="superheroes">
						<img src="img/MMMultiply_EA_FIFA19_08.jpg">
					</a>
					
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply_EA_FIFA19_02.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply_EA_FIFA19_02.jpg">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_EA_FIFA19_03.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply_EA_FIFA19_03.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_EA_FIFA19_04.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply_EA_FIFA19_04.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply_EA_FIFA19_05.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply_EA_FIFA19_05.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply_EA_FIFA19_06.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply_EA_FIFA19_06.jpg">
					</a>
				</div>
				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_EA_FIFA19_07.jpg" data-lightbox="superheroes">

						<img src="img/MMMultiply_EA_FIFA19_07.jpg">
					</a>
				</div>
				

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">


		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="bundesliga.php" data-pageid="content-page" class="ajax-link">
				<span>Bundesliga</span>
				<img src="img/MMMultiply_BundesligaTeaser_03.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="superheroes.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes-09.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="wild-launch-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Wild Launch Campaign</span>
				<img src="img/MMMultiply-WildLaunchCampaign-02.jpg">
			</a>
		</div>
	</div>
</section>