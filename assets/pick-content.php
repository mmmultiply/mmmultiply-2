<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Pick Rebrand</h1>
			<h4 class='content-filter__sub-title'>
				Bringing some style and sophistication to one of the country's favourite channels.
			</h4>			
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/179073841?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>
						We created a suite of idents that reflected the breadth of the channels content, from the light hearted to the heart pounding, that could be used across the days schedule tying in nicely with the programming.
					</p>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-popcorn-01.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-popcorn-01.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-popcorn-02.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-popcorn-02.jpg">
					</a>
				</div>
				

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/179075027" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-eye-02.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-eye-02.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-eye-01.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-eye-01.jpg">
					</a>
				</div>


				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/179075432" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-police-01.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-police-01.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-police-02.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-police-02.jpg">
					</a>
				</div>


				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/179073581" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>	

				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-cat-01.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-cat-01.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-Pick-cat-02.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-cat-02.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">

					<p>
						The ID’s focus on the everyday but intensify these scenario’s using super slow-motion making the viewers life more vibrant and interesting reflecting the content on Pick that makes everyday more exciting.	
					</p>		

				</div>

				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-Pick-logo-grad.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-logo-grad.jpg">
					</a>
				</div>


				<div class="col-12 content__item init-load inview">
					

					<a href="img/MMMultiply-Pick-guides2.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-guides2.jpg">
					</a>

				
				</div>

				<div class="col-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-Pick-logo.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-logo.jpg">
					</a>
				</div>

				<div class="col-6 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-Pick-guides.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-guides.jpg">
					</a>
				</div>
			

				<div class="col-12 content__item init-load inview">

					<a href="img/MMMultiply-Pick-OSP-menu.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-OSP-menu.jpg">
					</a>

				</div>

				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-Pick-OSP-endboard.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-OSP-endboard.jpg">
					</a>
				</div>

				<div class="col-12 content__item content__item--img init-load inview">
					<a href="img/MMMultiply-Pick-OSP-dve.jpg" data-lightbox="Pick">
						<img src="img/MMMultiply-Pick-OSP-dve.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">

					<p>For the on screen packaging, a changing set of colours reflected the different day parts, from the warm glow of the dawn to the deep blue of the night that allows the packaging to reflect the content.</p>

				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="bbc-brit.php" data-pageid="content-page" class="ajax-link">
				<span>BBC Brit Rebrand</span>
				<img src="img/MMMultiply-BBCBrit-01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single  projects__single--thumb init-load inview">
			<a href="challenge.php" data-pageid="content-page" class="ajax-link">
				<span>Challenge Rebrand</span>
				<img src="img/MMMultiply-Challenge-Modern_1.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="fox-plus.php" data-pageid="content-page" class="ajax-link">
				<span>FOX+ Brand</span>
				<img src="img/MMMultiply-FOX-Plus.jpg">
			</a>
		</div>
	</div>
</section>