<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Suicide Squad</h1>

			<h4 class='content-filter__sub-title'>
				Show packaging for some of the worst criminals in Superhero history.
			</h4>
		
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">
				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/179218434" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">
					<p>
						We were asked by our good friends at Sky to create a full graphic package for the Sky Cinema special 'Suicide Squad DC Villains Uncovered'.
					</p>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SS_Joker.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS_Joker.jpg">
					</a>
				</div>


				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SS-Joker2.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS-Joker2.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SS_Deadshot.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS_Deadshot.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SS_Boomerang.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS_Boomerang.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SS_HarleyQuinn.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS_HarleyQuinn.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SS_Enchantress.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS_Enchantress.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SS_ElDiablo.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS_ElDiablo.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-SS_RickFlag.jpg" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS_RickFlag.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 push-md-3 content__item init-load inview">
					<p>
						The show focused on the characters from the upcoming film, getting to know their personalities and backstories, so we created a dossier on each villain that was used as a bumper within the show, and a base for the title sequence.
					</p>
				</div>

				<div class="col-6 col-sm-3 content__item init-load inview">
					<a href="img/MMMultiply-SS-Deadshot-Icon.png" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS-Deadshot-Icon.png">
					</a>
				</div>
				<div class="col-6 col-sm-3 content__item init-load inview">
					<a href="img/MMMultiply-SS-Enchantress-Icon.png" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS-Enchantress-Icon.png">
					</a>
				</div>
				<div class="col-6 col-sm-3 content__item init-load inview">
					<a href="img/MMMultiply-SS-joker-icon.png" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS-joker-icon.png">
					</a>
				</div>
				<div class="col-6 col-sm-3 content__item init-load inview">
					<a href="img/MMMultiply-SS-Harley-Quinn-Icon.png" data-lightbox="Suicide Squad">
						<img src="img/MMMultiply-SS-Harley-Quinn-Icon.png">
					</a>
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-sm-9 offset-sm-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="justice-league-uncovered.php" data-pageid="content-page" class="ajax-link">
				<span>Justice League Uncovered</span>
				<img src="img/MMMultiply_SkyCinema_JusticeLeauge_ProfileBatman.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="sky-cinema-superheroes-2019.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes 2019</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes2019-01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="dc-heroes.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema DC Heroes</span>
				<img src="img/MMMultiply_SkyCinema_DCHeroes_01.jpg">
				
			</a>
		</div>
	</div>
</section>