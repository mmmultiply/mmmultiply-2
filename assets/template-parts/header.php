<!--[if lt IE 10]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

	<section class="loader">
	</section>

	<div class="ajax-progress">
	</div>

	<header class="container-fluid">

		<nav class="row flex-row-reverse">

			<div class="col-12 col-lg-6">
				<a class="ajax-link index-link" data-pageid="index-content" href="index.php">
					<h1 class="header__logo">
						MMMultiply	
					</h1>
				</a>
			</div>

			<div class="col-6 col-lg-6" id="navbar">
				<ul class="row flex-row header__links">
					<div class="col-12">
						<a>
							<div class="mobile-icon">
								<div class="menu-stripe"></div>
								<div class="menu-stripe"></div>
								<div class="menu-stripe"></div>
							</div>
						</a>
						<li class="about-link"><a class="ajax-link link-navigation" data-pageid="about-page" href="about.php">About</a></li>
						<li class="work-link"><a class="ajax-link link-navigation" data-pageid="projects-page" href="work.php">Work</a></li>
						<li class="feed-link"><a class="link-navigation" target="_blank" href="https://twitter.com/mmmultiply">Feed</a></li>
<!-- 						<li class="studio-link"><a class="ajax-link link-navigation" data-pageid="studio-page" href="studio.php">Studio</a></li> -->
						<!-- <li class="experiments-link"><a class=" link-navigation" href="https://www.instagram.com/mmmultiply/?hl=en">Experiments</a></li>
						 -->
					</div>
				</ul>
			</div>

		</nav>
	</header>

