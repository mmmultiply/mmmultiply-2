	<canvas id="intro-canvas">
	</canvas>

	<div class="contact-link"><a class="link-navigation">Contact</a></div>

	<footer class="contact-info container-fluid">
		<div class="row align-content-start flex-row flex-wrap">

			

			<div class="col-12 col-xl-3 footer-logo">
				<img src="img/grey-x-lg.svg">
			</div>

			<div class="col-12 col-xl-9">
				<div class="row">

					<div class="col-6 col-md-6 col-lg-3 contact-info__item">
						<h3 class="font-regular">Find Us</h3>
						<a class="link-navigation link-navigation--block" target="_blank" href="https://www.google.com/maps?ll=51.495729,-0.072627&z=16&t=m&hl=en-GB&gl=GB&mapclient=embed&cid=2253029466544699854">
							<p>
								MMMultiply<br>
								90-92 Spa Road<br>
								London<br>
								SE16 3QT<br>
							</p>
						</a>
					</div>

					<div class="col-6 col-md-6 col-lg-3 contact-info__item">
						<h3 class="font-regular">Talk To Us</h3>
						<a class="link-navigation link-navigation--block" href="tel:+44 (0) 203 870 1878">
							<p>+44 (0) 203 870 1878</p>
						</a>
					</div>
					
					<div class="col-6 col-md-6 col-lg-3 contact-info__item">
						<h3 class="font-regular">Email us</h3>
						<a class="link-navigation link-navigation--block" href="mailto:hello@mmmultiply.com">
							<p>hello@mmmultiply.com</p>
						</a>
					</div>
					
					<div class="col-6 col-md-6 col-lg-3 contact-info__item">
						<h3 class="font-regular">Follow us</h3>
						<a class="link-navigation link-navigation--block" href="https://twitter.com/mmmultiply">
							<p>Twitter</p>
						</a>
						<a class="link-navigation link-navigation--block" href="https://www.instagram.com/mmmultiply/?hl=en">
							<p>Instagram</p>
						</a>
						<a class="link-navigation link-navigation--block" href="https://uk.linkedin.com/company/mmmultiply?trk=ppro_cprof">
							<p>Linkedin</p>
						</a>
						<a class="link-navigation link-navigation--block" href="https://vimeo.com/mmmultiply">
							<p>Vimeo</p>
						</a>
						<a class="link-navigation link-navigation--block" href="https://www.behance.net/mmmultiply">
							<p>Behance</p>
						</a>

					</div>
				</div>
				
				<div class="row">
					<div class="col-12 col-md-10  copyright">
						<p>© 2018 MMMultiply Ltd. All Rights Reserved. <a href="privacy-policy.php" data-pageid="content-page" class="ajax-link link-navigation">Privacy Policy</a></p>
					</div>
				</div>
			</div>
			
			
			
		</div>
	</footer>

	<div id="cookieConsent">
	    This website is using cookies.<br><a href="privacy-policy.php" data-pageid="content-page" class="ajax-link link-navigation">More info</a><a class="cookieConsentOK link-navigation">That's Fine</a>
	</div>

	