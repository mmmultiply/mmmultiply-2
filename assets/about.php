<?php 
  include 'template-parts/header-html.php';
?>

<body id="about-page">

<?php 
  include 'template-parts/header.php';
  include 'about-content.php';
  include 'template-parts/footer.php';
?>

</body>

<?php 
  include 'template-parts/footer-html.php';
?>