<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Justice League Uncovered</h1>
			<p>
				Show packaging for documentary on Sky Cinema.
			</p>
			<p>
				Sky Cinema’s documentary takes a special look at DC's star-studded superhero adventure, getting to know the characters origins and personalities. We were invited to create the show packaging including title sequence and character profiles.
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">

					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/284760057" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>

				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_SkyCinema_JusticeLeauge_Bumper.jpg" data-lightbox="justice">
						<img src="img/MMMultiply_SkyCinema_JusticeLeauge_Bumper.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_SkyCinema_JusticeLeauge_ProfileBatman.jpg" data-lightbox="justice">
						<img src="img/MMMultiply_SkyCinema_JusticeLeauge_ProfileBatman.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_SkyCinema_JusticeLeauge_LowerThird.jpg" data-lightbox="justice">
						<img src="img/MMMultiply_SkyCinema_JusticeLeauge_LowerThird.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply_SkyCinema_JusticeLeauge_ProfileWW.jpg" data-lightbox="justice">
						<img src="img/MMMultiply_SkyCinema_JusticeLeauge_ProfileWW.jpg">
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-3 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="suicide-squad.php" data-pageid="content-page" class="ajax-link">
				<span>Suicide Squad Packaging</span>
				<img src="img/MMMultiply-SS-Cover.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb  init-load inview">
			<a href="dc-heroes.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema DC Heroes</span>
				<img src="img/MMMultiply_SkyCinema_DCHeroes_01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="sky-fy.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Fy</span>
				<img src="img/MMMultiply-SKY-FY-Endslate.jpg">
			</a>
		</div>
	</div>
</section>