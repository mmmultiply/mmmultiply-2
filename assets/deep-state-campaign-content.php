<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Deep State S1 Campaign</h2>
			<h4 class='content-filter__sub-title'>Above-the-line campaign for FOX's new espionage thriller.</h4>	
		</div>
		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">

						<iframe src="https://player.vimeo.com/video/267607175?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 content__item init-load inview">
					<h3>Fact. Fiction. The Truth.</h3>
				</div>

				<div class="col-12 content__item init-load inview">
					
					<a href="img/MMMultiply-DeepStateATL-06.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-06.jpg">
					</a>

				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">

					<p>The Deep State is everywhere, lurking beneath the surface, controlling governments, the media and everything in our lives, nothing is what it seems. Welcome to FOX’s new espionage thriller Deep State.</p>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateATL-08.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-08.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateATL-09.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-09.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateATL-07.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-07.jpg">
					</a>
				</div>

				<div class="col-12 content__item init-load inview">
										
					<a href="img/MMMultiply-DeepStateATL-05.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-05.jpg">
					</a>
	
				</div>

				<div class="col-12 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateATL-04.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-04.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateATL-02.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-02.jpg">
					</a>
				</div>
				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-DeepStateATL-03.jpg" data-lightbox="deep state atl">
						<img src="img/MMMultiply-DeepStateATL-03.jpg">
					</a>
				</div>

				<div class="col-12 col-md-6 offset-md-3 content__item init-load inview">

					<p>Working closely with the FOX UK team we developed the creative direction for the campaign and created the European toolkit for territory roll out.</p>
					<p>To convey the sense of things being out of the viewers control and not what they seem, we developed the frosted glass creative and a title treatment that would convey this, along with a claustrophobic feel.</p>
					<p>This offered the chance to obscure and reveal content at our discretion, offering glimpses behind the Deep State and building interest and excitement as the campaign progressed.</p>
					
				</div>

			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
		
	</div>
	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="deep-state-titles.php" data-pageid="content-page" class="ajax-link">
				<span>Deep State Titles</span>
				<img src="img/MMMultiply-DeepStateTitles-01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="wild-launch-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>National Geographic Wild Launch Campaign</span>
				<img src="img/MMMultiply-WildLaunchCampaign-02.jpg">
				
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="returntothenile-campaign.php" data-pageid="content-page" class="ajax-link">
				<span>Return to the Nile Campaign</span>
				<img src="img/MMMultiply-ReturnToTheNileCampaign-01.jpg">
			</a>
		</div>
	</div>
</section>