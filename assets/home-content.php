<section class="container ajax">

		<div class="hero-content">
			<div class="row row-flex hero-content__message align-content-center">

				<div class="col-6 col-md-7">
					<h1><span class="font-regular">We</span> MMMultiply</h1>
				</div>
				<div class="col-6 col-md-7 hero-content__message--catergories">
					<h1 class="font-regular top-cat">brands</h1>
					<h1 class="font-regular">campaigns</h1>
					<h1 class="font-regular">motion</h1>
					<h1 class="font-regular">digital</h1>
				</div> 
				<div class="col-6 col-md-7">
					<h1 class="font-regular">through creative</br>collaboration</h1>
				</div>

			</div>
		</div>
			
		<div class="row flex-column align-items-end justify-content-center projects">

			<div data-count="1" class="projects__single col-6 col-md-5 init-load" data-cat="brand">
				<a href="#" class="ajax-link">
					<img src="img/MMMultiply-BBCBrit-01.jpg">
					<span>BBC Brit</span>
				</a>
			</div>

			<div data-count="3" class="projects__single col-6 col-md-5 init-load" data-cat="campaign">
				<a href="#" class="ajax-link">
					<img src="img/MMMultiply-DeepStateATL-01.jpg">
					<span>Deep State Campaign</span>
				</a>
			</div>

			<div data-count="4" class="projects__single col-6 col-md-5 init-load" data-cat="motion">
				<a href="#" class="ajax-link">
					<img src="img/MMMultiply-DeepStateTitles-01.jpg">
					<span>Deep State Title Sequence</span>
				</a>
			</div>

			<div data-count="2" class="projects__single col-6 col-md-5 init-load" data-cat="brand">
				<a href="#" class="ajax-link">
					<img src="img/MMMultiply-Challenge-Logo.jpg">
					<span>Challenge Rebrand</span>
				</a>
			</div>
			
			
			<div  data-count="5" class="projects__single col-6 col-md-5 init-load " data-cat="brand">
				<a href="#" class="ajax-link">
					<img src="img/MMMultiply-Fox-Plus-logo.jpg">
					<span>Fox Plus</span>
				</a>
			</div>
			<div data-count="6" class="projects__single col-6 col-md-5 init-load " data-cat="motion">
				<a href="#" class="ajax-link">
					<img src="img/MMMultiply-SkyCinema_SuperHeroes_01.jpg">
					<span>Sky Cinema Super Heroes</span>
				</a>
			</div>

			<div data-count="7" class="projects__single col-6 col-md-5 init-load " data-cat="na">
				<a href="/work.php" data-pageid="projects-page" class="ajax-link">
					<img src="img/MMMultiply-cross.jpg">
					<span>See all work</span>
				</a>
			</div>

		</div>
		
	</section>