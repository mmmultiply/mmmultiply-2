<section class="ajax">

	<div class="fake-logo">
	</div>

	<section id="section2" class="container-fluid collab-module">
		<div class="row flex-row">
			<div class="col-12 collab-module__content flex-row justify-content-center align-items-center flex-wrap">
				<div class="collab-message">
					We are an award-winning independent creative studio with <span class="font-bold">collaboration</span> and <span class="font-bold">experimentation</span> at the heart of everything we do.
				</div>
			</div>
			<a href="#intro">
				<div class="collab-module__next">
				</div>
			</a>
		</div>
	</section>
<!-- 
	<section id="intro" class="container introduction">
		<div class="row flex-row">

			<div class="col-12">
				<h3>
					Who We Are
				</h3>
			</div>
			<div class="col-12 offset-lg-3 col-lg-6">
				<p>
					Our multi-disciplinary team works together to conceive and execute projects for brands, broadcasters and partners alike, crafting coherent concepts, bold copy, and beautiful creative that resonates with their audience whatever the media.
				</p>
				<p>
					While our drive for collaboration within the team is core to how we solve creative problems, we are just as committed to developing how this extends to the way we partner with clients.
				</p>
			</div>
			
		</div>
	</section> -->

	<section id="intro" class="container-fluid details no-gutters">
		<div class="row flex-md-row-reverse align-items-center">
			<div class="col-12 col-lg-6">
				<img src="img/About_WhoWeAre.jpg">
			</div>
			<div class="col-12 col-lg-6 push-xl-1 col-xl-4 details__text">
				<h4>
					Who we are
				</h4>
				<p>
					Our multi-disciplinary team works together to conceive and execute projects for brands, broadcasters and partners alike, crafting coherent concepts, bold copy, and beautiful creative that resonates with their audience whatever the media.
				</p>

			</div>
		</div>
	</section>

	<section class="container-fluid details services no-gutters">
		<div class="row flex-md-row align-items-center">

			<div class="col-12 col-lg-6 services__images">

				<div class="branding active">
					<a href="fox-plus.php" class="ajax-link" data-pageid="content-page">
						<img class="" src="img/MMMultiply_services_branding.jpg">
					</a>
				</div>

				<div class="advertising">
					<a href="national-geographic-magazine.php" class="ajax-link" data-pageid="content-page">
						<img class="" src="img/MMMultiply_services_natgeo.jpg">
					</a>
				</div>

				<div class="design">
					<a href="fox-agentcarter.php" class="ajax-link" data-pageid="content-page">
						<img class="" src="img/MMMultiply_services_design.jpg">
					</a>
				</div>

				<div class="vfx">
					<a href="bbc-brit.php" class="ajax-link" data-pageid="content-page">
						<img class="" src="img/MMMultiply_services_vfx.jpg">
					</a>
				</div>

				<div class="digital">
					<a href="deep-state-campaign.php" class="ajax-link" data-pageid="content-page">
						<img class="" src="img/MMMultiply_services_digital.jpg">
					</a>
				</div>


			</div>


			<div class="col-12 col-lg-6 offset-xl-1 col-xl-4 details__text">

				<h4>
					Our Services
				</h4>

				<ul class="services__list">
					<li class="branding active">
						Branding	
					</li>
					<li class="advertising">
						Advertising	
					</li>
					<li class="design">
						Design
					</li>
					<li class="vfx">
						VFX &amp; 3D	
					</li>
					<li class="digital">
						Digital	
					</li>
				</ul>

				<div class="services__content">

					<p class="branding">
						A clear understanding of the product and audience is key to defining new brands and refreshing existing ones with relevant creative that succeeds in enhancing the brand experience. We dedicate just as much time to research as we do creating flexible visual identities that allow brands to grow with clarity and consistency.
					</p>

					<p class="advertising">
						Whether it’s conceiving 360º campaign concepts, managing the roll out of national press and outdoor campaigns, or creating digital and social adverts with cut through, we approach all our campaigns in the same way we approach a branding project, by keeping a clear focus on the core brand needs and visuals that amplify the message.
					</p>

					<p class="design">
						While concepts are key, we are all driven to execute ideas with precision and care. Visual identities created for global brands, social units with cut through, motion graphics that resonate, clear communication of ideas needs balance and a real consideration for both the big picture and the smallest details.
					</p>

					<p class="vfx">
						Integral to much of the moving image work we produce, visual effects and high end 3D run through the core of our technical output. Creating everything from photo-real products to impossible worlds also allows us to think beyond two dimensions for all our work.
					</p>

					<p class="digital">
						Digital process is at the heart of our platform for work, using the latest tools that allow collaboration across the studio. This extends to developing digital experiences that combine our keen eye for design and motion work and other bespoke executions from websites to advertising.
					</p>
				</div>

			</div>
			
		</div>
	</section>





	


	<section class="container-fluid details no-gutters">
		<div class="row flex-md-row-reverse align-items-center">

			<div class="col-12 col-lg-6 details__logos">

				<div class="row flex-row justify-content-center ">
					<div class="col-4 col-md-3">
						<img src="img/logos/A+E.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/BBC_logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/DSC_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/EA_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/eno_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/FOX_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/fremantle_logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/Money2020_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/MTV_logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/NG_LOGO.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/NowTV_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/Sky_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/UKTV_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/universal_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/Villain_Logo.png">
					</div>
					<div class="col-4 col-md-3">
						<img src="img/logos/VirginActive_logo.png">
					</div>
					

				</div>

			</div>

			<div class="col-12 col-lg-6 push-xl-1 col-xl-4 details__text">


				<h4>
					Forging relationships
				</h4>
				<p>While our drive for collaboration within the team is core to how we approach and solve creative problems, we are just as committed to developing how this extends to the way we partner with clients.</p>

				<p>This close collaboration with our clients and partners allow us to better understand their business needs and creative aspirations, build strong bonds, and forge long lasting relationships time and time again.</p>



			</div>
			
		</div>
	</section>

	<section class="container-fluid details no-gutters">
		<div class="row flex-row align-items-center">

			<div class="col-12 col-lg-6">
				<img src="img/About_TheMMMultiplyWay.jpg">
			</div>

			<div class="col-12 col-lg-6 offset-xl-1 col-xl-4 details__text">

				<h4>
					The MMMultiply Way
				</h4>
				<p>Driven by a desire to experiment and grow, we strive to continually explore new creative and technological ground, and constantly expand and redefine what it means to MMMultiply.</p>

				<p>We are never satisfied, and we will never stand still.</p>

<!-- 				<p>To find out more about our process, take a look behind the scenes in the studio <a href="studio.php" class="ajax-link" data-pageid="studio-page">here</a>.</p> -->
			</div>
			
		</div>
	</section>

	<section class="container-fluid details no-gutters">
		<div class="row flex-md-row-reverse align-items-center">

			<div class="col-12 col-lg-6">
				<img src="img/About_JoinUs.jpg">
			</div>

			<div class="col-12 col-lg-6 push-xl-1 col-xl-4 details__text">

				<h4>
					Join Us
				</h4>
				<p>
					
					If you think you’d like to be part of a growing team of passionate designers, we’re always on the look out for talented individuals who can bring something new to the team. Send on a link to your portfolio to <a href="mailto:careers@mmmultiply.com">careers@mmmultiply.com</a> and we’ll be in touch.
				</p>
				<p>
					Even if you’re not looking for a full time position, if you’d like to be added to our freelance contractor list, drop us an email to <a href="mailto:hello@mmmultiply.com">hello@mmmultiply.com</a> with a link to some recent work and we’ll add you.				
				</p>
			</div>
			
		</div>
	</section>





	<!-- <section class="container vaccancies mt-gutter">
		<div class="row">

			<div class="col-12">
				<h3>
					Vacancies
				</h3>
			</div>
			
			<div class="col-12 offset-lg-3 col-lg-6 mt-gutter">
								
				<div class="row mt-gutter">
					<div class="col-12 col-sm-6 vaccancies__job">
						Middle Weight 3d &amp; 
						Motion Designer
						<a href="https://ifyoucouldjobs.com/">
							<button>
								Find out more +
							</button>
						</a>
					</div>

					<div class="col-12 col-sm-6 vaccancies__job">
						Junior Graphic Designer
						<a href="https://ifyoucouldjobs.com/">
							<button>
								Find out more +
							</button>
						</a>
					</div>
				</div>

				
			</div>

		</div>
	</section> -->

</section>


