<section class="container-fluid ajax">

			
	<div class="row flex-column align-items-end justify-content-center projects">

		<div  data-count="1" class="projects__single col-12 col-sm-9 init-load " data-cat="campaign">
			<a href="deep-state-s2-campaign.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply-DeepStateS2-KA-Thumb.jpg">
				<span>Deep State S2 Campaign</span>
			</a>
		</div>

		<div data-count="2" class="projects__single col-12 col-sm-9 init-load" data-cat="motion">
			<a href="sky-cinema-superheroes-2019.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply-SkyCinema-SuperHeroes2019-01.jpg">
				<span>Sky Cinema Superheroes 2019</span>
			</a>
		</div>

		<div data-count="3" class="projects__single col-12 col-sm-9 init-load" data-cat="brand">
			<a href="bbc-lifestyle.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply-BBCLifestyle-ID03-02.jpg">
				<span>BBC Lifestyle Rebrand</span>
			</a>
		</div>

		<div data-count="4" class="projects__single col-12 col-sm-9 init-load" data-cat="motion">
			<a href="deep-state-titles.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply_DeepState_titles.jpg">
				<span>Deep State Title Sequence</span>
			</a>
		</div>

		<div data-count="5" class="projects__single col-12 col-sm-9 init-load" data-cat="campaign">
			<a href="wild-launch-campaign.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply-WildLaunchCampaign-01.jpg">
				<span>NG Wild Launch Campaign</span>
			</a>
		</div>
		
		<div data-count="6" class="projects__single col-12 col-sm-9 init-load " data-cat="motion">
			<a href="ea-fifa19.php" data-pageid="content-page" class="ajax-link">
				<img src="img/MMMultiply_EA_FIFA19_08.jpg">
				<span>EA FIFA 19</span>
			</a>
		</div>

<!-- 		<div data-count="7" class="projects__single col-12 col-sm-9 init-load "  data-cat="brand">
			<a href="studio.php" data-pageid="studio-page" class="ajax-link">
				<img src="img/MMMultiply-Studio.jpg">
				<span>The Studio</span>
			</a>
		</div> -->

		<div data-count="7" class="projects__single col-12 col-sm-9 init-load show-reel" data-cat="brand">
			<img src="img/show-reel.jpg">
			<span>Watch our latest reel</span>
		</div>

		<div class="show-reel__container">
			<div class="show-reel__spacer">
				<div class="show-reel__close"></div>
				<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/255037849?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
			</div>
		</div>

	</div>


	<div class="hero-content init-load">
		<div class="row flex-sm-colum hero-content__message align-content-center">

			<div class="col-12 col-sm-10 offset-md-1 col-md-7 col-xl-6">
				<span>We</span> <span>MMMultiply</span>
				<span class="cat cat--brand" data-catmessage="brands">brands</span>
				<span class="cat cat--campaign" data-catmessage="campaigns">campaigns</span>
				<span class="cat cat--motion" data-catmessage="motion">motion</span>
				<span class="cat cat--digital" data-catmessage="digital">digital</span>
				<br/>
				<span>through creative collaboration</span>
				<br/>
			</div>

			<div class="bg-cross d-none d-md-block" >
			</div>
			

			<div class="col-12 col-sm-10 offset-md-1 col-md-11 ">

				<a href="about.php" data-pageid="about-page" class="hero-content__link link-navigation ajax-link">
					Find out how
				</a>
			</div>

		</div>
	</div>


</section>