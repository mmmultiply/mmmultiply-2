<section class="container-fluid ajax">
	<div class="row flex-md-row align-items-start">

		<div class="content-filter col-12 col-lg-3 init-load inview">
			<h1 class="h5">Sky Cinema Now Showing</h1>
			<p>
				Sky Cinema is constantly looking to bring the benefits of the Cinema to its customers, giving them the best cinematic experience possible. They were preparing to release a selection of Original Films on Sky Cinema the same day they went on general release in cinemas across the country.

			</p>
			<p>
				We teamed up with Sky Creative to create a promo that illustrated this concept clearly and was easily updated for new titles released later in the year.
			</p>
			<p>
				All the locations featured in the promo were built from scratch using image stock and 3D.
			</p>
		</div>

		
		<div class="content col-12 col-lg-9 ">
			<div class="row ">

				<div class="col-12 content__item content__item--img init-load inview">
					<div style="padding:56.25% 0 0 0;position:relative;">
						<iframe src="https://player.vimeo.com/video/257115658?background=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-NowShowing-02.jpg" data-lightbox="NowShowing">
						<img src="img/MMMultiply-SkyCinema-NowShowing-02.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-NowShowing-01.jpg" data-lightbox="NowShowing">
						<img src="img/MMMultiply-SkyCinema-NowShowing-01.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-NowShowing-06.jpg" data-lightbox="NowShowing">
						<img src="img/MMMultiply-SkyCinema-NowShowing-06.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-NowShowing-03.jpg" data-lightbox="NowShowing">
						<img src="img/MMMultiply-SkyCinema-NowShowing-03.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-12 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-NowShowing-04.jpg" data-lightbox="NowShowing">
						<img src="img/MMMultiply-SkyCinema-NowShowing-04.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-NowShowing-05.jpg" data-lightbox="NowShowing">
						<img src="img/MMMultiply-SkyCinema-NowShowing-05.jpg">
					</a>
				</div>

				<div class="col-12 col-sm-6 content__item init-load inview">
					<a href="img/MMMultiply-SkyCinema-NowShowing-07.jpg" data-lightbox="NowShowing">
						<img src="img/MMMultiply-SkyCinema-NowShowing-07.jpg">
					</a>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12 col-lg-9 offset-lg-3 related-work__title">
			<h3 class="h5">
				Related Work
			</h3>
		</div>
	</div>

	<div class="row related-work projects projects--3">
		<div class="col-12 col-sm-4 col-lg-3 offset-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="sky-cinema-superheroes-2019.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes 2019</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes2019-01.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="heroes-and-villains.php" data-pageid="content-page" class="ajax-link">
				<span>Heroes & Villains Promo</span>
				<img src="img/MMMultiply-heroesVillains-02.jpg">
			</a>
		</div>
		<div class="col-12 col-sm-4 col-lg-3 projects__single projects__single--thumb init-load inview">
			<a href="superheroes.php" data-pageid="content-page" class="ajax-link">
				<span>Sky Cinema Superheroes</span>
				<img src="img/MMMultiply-SkyCinema-SuperHeroes-09-thumb.jpg">
			</a>
		</div>
	</div>
</section>