var gulp             = require('gulp'),
    gutil            = require('gulp-util'),
    compass          = require('gulp-compass'),
    minify           = require('gulp-minify'),
    connect          = require('gulp-connect-php'),
    browserSync      = require('browser-sync'),
    uglify           = require('gulp-uglify'),
    plumber          = require('gulp-plumber'),
    notify           = require('gulp-notify'),
    autoprefixer     = require('gulp-autoprefixer'),
    rename           = require('gulp-rename'),
    autoprefixer     = require('gulp-autoprefixer'),
    concat           = require('gulp-concat');
    imagemin         = require('gulp-imagemin');



var jsSources        = ['scripts/vendor/*.js', 'scripts/vendor/**/*.js', 'scripts/main.js', 'scripts/modules/*.js'],
    sassSources      = ['styles/*.scss','styles/_partials/*.scss'],
    htmlSources      = ['*.html'],
    phpSources       = ['template-parts/*.php', '*.php'];

var outputDir        = 'assets',
    imgDir           = 'assets/img',
    fontDir          = 'assets/fonts'
    templateDir      = 'assets/template-parts';

var reload           = browserSync.reload;

var notifyInfo = {
	title: 'Gulp',
	icon: 'icon.png'
};

var plumberErrorHandler = { errorHandler: notify.onError({
		title: notifyInfo.title,
		icon: notifyInfo.icon,
		message: "Error: <%= error.message %>"
	})
};

gulp.task('log', function() {
  gutil.log('== My First Task ==')
});

gulp.task('copy', function() {
  gulp.src('*.html')
  .pipe(gulp.dest(outputDir));

  gulp.src('fonts/**')
  .pipe(gulp.dest(fontDir));
});

gulp.task('imgcopy', function() {
  gulp.src('img/**')
  .pipe(imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({
      plugins: [
        {removeViewBox: true},
        {cleanupIDs: false}
      ]
    })
  ]))
  .pipe(gulp.dest(imgDir));

});

gulp.task('compass', function() {

  gulp.src('styles/*.scss')
    .pipe(plumber())
    .pipe(compass({

      sass: 'styles',
      image: 'img',
      style: 'nested'
    }))
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(outputDir))
    .pipe(reload({stream: true}));
});

gulp.task('js', function() {
  gulp.src(jsSources)
  .pipe(plumber())
  // .pipe(uglify())
  .pipe(concat('script.js'))
  .pipe(gulp.dest(outputDir))
  .pipe(minify())
  .pipe(gulp.dest(outputDir))
  .pipe(reload({stream: true}));
});

gulp.task('php', function() {
  gulp.src('*.php')
  .pipe(plumber())
  .pipe(gulp.dest(outputDir));

  gulp.src('template-parts/*.php')
  .pipe(plumber())
  .pipe(gulp.dest(templateDir))
  .pipe(reload({stream: true}));
});

gulp.task('watch', function() {
  
  gulp.watch(phpSources, ['php']);
  gulp.watch(jsSources, ['js']);
  gulp.watch(sassSources, ['compass']);
  gulp.watch(htmlSources, ['copy']);
  gulp.watch('img/**', ['imgcopy']);

});

gulp.task('connect', function() {

  connect.server({
    base: outputDir,
  }, function (){
    browserSync({
      proxy: 'localhost:8000'
    });
  });

});

gulp.task('default', ['js', 'watch', 'compass', 'copy', 'imgcopy', 'php', 'connect']);